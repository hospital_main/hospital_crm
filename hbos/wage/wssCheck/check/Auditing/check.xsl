<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
			<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true" >职工编码</th> 
				<th noWrap="true" >职工姓名</th>
				<th noWrap="true" >考勤性质</th>
				<th noWrap="true" >人员类别</th>
				<th noWrap="true" >考勤类别</th>
				<xsl:for-each select="/root/tbody/tr[td[1]='']/td[position() &gt; 5]">
					<th noWrap="true" ><xsl:value-of select="."/></th>
				</xsl:for-each>
		  </tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[td[1] != '']">
				<tr>     
					<xsl:for-each select="td">
						<td>
							<xsl:value-of select="."/>
						</td>	
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
