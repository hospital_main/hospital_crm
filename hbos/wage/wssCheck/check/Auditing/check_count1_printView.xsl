<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
    <thead>
      	<tr noWrap='true' class='mainHead'>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 0]">
				 <td nowrap='true' width='80'><xsl:value-of select="."/></td>
			</xsl:for-each>
      	</tr>
      	
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
		<xsl:variable name="EMP_CODE" select="td[1]"/>
		<xsl:variable name="CHECK_ID" select="./pk/check_id"/>
		<xsl:variable name="TD2" select="td[2]" />
		<xsl:variable name="TD3" select="td[3]" />
		<xsl:variable name="spanvalue" select=" concat($EMP_CODE,$TD2,$TD3)" />
		<xsl:variable name="cur_pos" select="position() + 2" />					 
		<xsl:variable name="rowspan" select="count(/root/tbody/tr[concat(td[1],td[2],td[3])= $spanvalue ])" />
        <tr>
          <xsl:for-each select="td[position() &gt; 0]">
          <xsl:variable name="NUM" select="position()-4"></xsl:variable>
		  <xsl:choose>
			<xsl:when test="position()=1"> 
				<xsl:if test="$cur_pos = 1">
					<xsl:if test="$spanvalue != ''">													
						<td rowspan="{$rowspan}" width='80'> 
							<xsl:value-of select="." />											
						</td>
					 </xsl:if>
				</xsl:if>
				
			<xsl:if test="$cur_pos &gt; 1">
				<xsl:if test="$spanvalue != concat(../../tr[$cur_pos - 1 ]/td[1],../../tr[$cur_pos - 1 ]/td[2],../../tr[$cur_pos - 1 ]/td[3]) ">
																	
						<td rowspan="{$rowspan}" width='80'> 
							<xsl:value-of select="." />											
						</td>
					</xsl:if>
					
					<xsl:if test="$spanvalue = concat(../../tr[$cur_pos - 1 ]/td[1],../../tr[$cur_pos - 1 ]/td[2],../../tr[$cur_pos - 1 ]/td[3]) ">
						<xsl:if test="$spanvalue != ''">
						<td align="center" style="display:none" >
						</td>
						</xsl:if>
						<xsl:if test="$spanvalue = ''">
						<td align="center" width='80'>
							<xsl:value-of select="."/>
						</td>
						</xsl:if>
					</xsl:if>
			</xsl:if>
			</xsl:when>
			<xsl:when test="position()=2"> 
				<xsl:if test="$cur_pos = 1">
					<xsl:if test="$spanvalue != ''">													
						<td rowspan="{$rowspan}" width='80'> 
							<xsl:value-of select="." />											
						</td>
					 </xsl:if>
				</xsl:if>
				
			<xsl:if test="$cur_pos &gt; 1">
				<xsl:if test="$spanvalue != concat(../../tr[$cur_pos - 1 ]/td[1],../../tr[$cur_pos - 1 ]/td[2],../../tr[$cur_pos - 1 ]/td[3]) ">
																	
						<td rowspan="{$rowspan}" width='80'> 
							<xsl:value-of select="." />											
						</td>
					</xsl:if>
					
					<xsl:if test="$spanvalue = concat(../../tr[$cur_pos - 1 ]/td[1],../../tr[$cur_pos - 1 ]/td[2],../../tr[$cur_pos - 1 ]/td[3]) ">
						<xsl:if test="$spanvalue != ''">
						<td align="center" style="display:none" >
						</td>
						</xsl:if>
						<xsl:if test="$spanvalue = ''">
						<td align="center" width='80'>
							<xsl:value-of select="."/>
						</td>
						</xsl:if>
					</xsl:if>
			</xsl:if>
			</xsl:when>
			<xsl:when test="position()=3"> 
				<xsl:if test="$cur_pos = 1">
					<xsl:if test="$spanvalue != ''">													
						<td rowspan="{$rowspan}" width='80'> 
							<xsl:value-of select="." />											
						</td>
					 </xsl:if>
				</xsl:if>
				
			<xsl:if test="$cur_pos &gt; 1">
				<xsl:if test="$spanvalue != concat(../../tr[$cur_pos - 1 ]/td[1],../../tr[$cur_pos - 1 ]/td[2],../../tr[$cur_pos - 1 ]/td[3]) ">
																	
						<td rowspan="{$rowspan}" width='80'> 
							<xsl:value-of select="." />											
						</td>
					</xsl:if>
					
					<xsl:if test="$spanvalue = concat(../../tr[$cur_pos - 1 ]/td[1],../../tr[$cur_pos - 1 ]/td[2],../../tr[$cur_pos - 1 ]/td[3]) ">
						<xsl:if test="$spanvalue != ''">
						<td align="center" style="display:none" >
						</td>
						</xsl:if>
						<xsl:if test="$spanvalue = ''">
						<td align="center" width='80'>
							<xsl:value-of select="."/>
						</td>
						</xsl:if>
					</xsl:if>
			</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<td><xsl:value-of select="."/></td>               
			</xsl:otherwise>
		  </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
	</root>
  </xsl:template>
</xsl:stylesheet>

