<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
		  <tr noWrap='true' class='mainHead'>
	      <th style=''><input type='checkbox'/></th>
				<th noWrap="true">记录单号</th>
				<th noWrap="true">记录日期</th>
	      <th noWrap="true">所属科室</th>
				<th noWrap="true">职工信息</th>
				<th noWrap="true">开始日期</th>
				<th noWrap="true">结束日期</th>
				<th noWrap="true">休假项目</th>
				<th noWrap="true">休假天数</th>
				<th noWrap="true">休假原因</th>
				<th noWrap="true">操作人</th>
				<th noWrap="true">状态</th>
      </tr>
  	</thead>
  	<tbody>

	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style=''>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=1">
							  <td  noWrap='true' >
							  <a tabindex="-1">
							    <xsl:attribute name="href" >
							    	javascript:view('update.html?load=&lt;id&gt;<xsl:value-of select="../td[1]"/>&lt;/id&gt;&lt;state&gt;<xsl:value-of select="../td[position()=11]"/>&lt;/state&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
							<xsl:when test="position()=8">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


