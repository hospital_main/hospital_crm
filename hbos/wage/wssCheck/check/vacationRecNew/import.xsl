<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
		  <tr onclick='changeapply()' noWrap='true' class='mainHead'>
	      <th style='width:5px;'><input type='checkbox'/></th>
				<th noWrap="true">申请单号</th>
				<th noWrap="true">申请日期</th>
				<th noWrap="true">职工信息</th>
	      <th noWrap="true">所属科室</th>				
				<th noWrap="true">休假天数</th>
				<th noWrap="true">休假说明</th>
				<th noWrap="true">审核人</th>
      </tr>
  	</thead>
  	<tbody>

	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center' onclick='changeapply()'   style='width:5px;'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=5">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


