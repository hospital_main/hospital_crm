<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>      
		   <tr noWrap='true' class='mainHead'>
				<th noWrap='true'></th>			
		  		<th noWrap='true'>部门</th>	
				<th noWrap='true'>工号</th>	
				<th noWrap='true'>姓名</th>	
				<th noWrap='true'>日期</th>	
				<th noWrap='true'>已有的其他部门日考勤数量</th>	
				<th noWrap='true'>未增加原因</th>	
		    </tr>
      
    </thead>
    
    <tbody>
    
	    <xsl:for-each select="/root/tbody/tr">
	    
		    <tr >
			    <td align='center'>
			    	<input type='radio' TABINDEX='-1' style='font-size:8px;'  name='select' >
				      <xsl:attribute name="value">
				          <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
					  </xsl:attribute>
					</input>
			    </td>
			    <xsl:for-each select="td"> 			    
			
			   <td>			    
			   		<xsl:value-of select="."/>
			    </td>
			 
			    </xsl:for-each>
		    </tr>
	    </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

