var showstop = 0, showlower = 0, texpands = 1;
// 全局变量 职工附属信息ridvalue
var ridvalue = "";
// 条件说明cond_desc 条件列表cond_list 条件内容cond
var cond_desc = "";
var cond_list = "";
var cond = "";
var wss_listpurview = "1";
 //条件名称
var cond_title = "";

function init() {// 初始化左侧树
	
	changeSalMonth();
	
	var pk = getPk();
	unitTree.expand = 1;
	unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect", pk);
	unitTree.init();
	
	getPurviewSelect();
	
	
	
    //初始化考勤开始时间为当前结账时间
  	window.xmlhttp.post('hr_att_wss_wageBatch_startDate',"","?isCheck=false");
  	
	var resText = window.xmlhttp._object.responseXml;
	if(resText.getElementsByTagName("td")[0]==null)
	{ 
		hr_slrSalPersonnelPersonnelSal_CreateSal.disabled=true;
		hr_att_wss_wageBatch_Bauthselect.disabled=true;
	 	alert("您还没有设置薪酬启用年月!");
	 	return;
	 }
	 getPurviewbutton();
	SalYear.setValue(resText.getElementsByTagName("td")[0].firstChild.nodeValue);
	SalMonth.setValue(resText.getElementsByTagName("td")[1].firstChild.nodeValue);
	
	//judgeAccountData();  //通过年月判断是否已经结账，从而判断"月初始化"、"批量调整"按钮是否可用
	isCheckWage();//判断月初始化按钮是否置灰.
	isCheckWage_batch();//判断批量调整是否置灰
}
function isCheckWage()
{
	var d=getValuePairBySql('wage_year_month_begin_end_date',"");//返回当前年月
				var year = d[0];
				var month  = d[1];
				
			//	var ddd=getValuePairBySql('wage_is_submit_state',"<year>"+year+"</year><month>"+SalMonth.value+"</month>");//是否有提交数据 1有0无
			//	var is_submit_state = ddd[0];
				var dd=getValuePairBySql('wage_is_check',"<year>"+SalYear.value+"</year><month>"+SalMonth.value+"</month>");//是否审核 1审核0未审核
				var is_check = dd[0];
				var ss=getValuePairBySql('wage_is_slry_num',"<userid>"+getUserID()+"</userid><year>"+SalYear.value+"</year><month>"+SalMonth.value+"</month>");//是否有考勤数据 =0 无 >0 有
				var is_wage_num = ss[0];
				var isSubmitFlag = getValuePairBySql("hr_attwageDataCheckInit_isAccount","<salyear>"+SalYear.value+"</salyear><salmonth>"+SalMonth.value+"</salmonth>");//是否结账
				//添加一个判断是否有表的权限
				var is_add = getValuePairBySql('hr_batch_month_start',"<userid>"+getUserID()+"</userid>");//是否有添加权限 >0 有 
				
				if(is_add[0]>0 && SalYear.value != '' && SalMonth.value != '' && year==SalYear.value && month==SalMonth.value &&  is_wage_num==0 && is_check==0 && isSubmitFlag[0]==0)
				{
					hr_slrSalPersonnelPersonnelSal_CreateSal.disabled=false;
					}
					else
						{
							hr_slrSalPersonnelPersonnelSal_CreateSal.disabled=true;
							}
	/*
	var dd=getValuePairBySql('wage_is_check_wage_wage',"<year>"+SalYear.value+"</year><month>"+SalMonth.value+"</month>");
					var is_check_wage = dd[0];
					if(is_check_wage==1)
					{
						hr_slrSalPersonnelPersonnelSal_CreateSal.disabled=true;
					}
					else
						{
							hr_slrSalPersonnelPersonnelSal_CreateSal.disabled=false;
							}
				var dd=getValuePairBySql('wage_is_check',"<year>"+SalYear.value+"</year><month>"+SalMonth.value+"</month>");
				var is_check = dd[0];
						
					if(is_check == 1)
					{
						
						perchange.disabled=true;
					}
					else
						{
							
							perchange.disabled=false;
							}*/
			
	}
	function isCheckWage_batch()//判断批量调整按钮是否置灰
{
	var d=getValuePairBySql('wage_year_month_begin_end_date',"");//返回当前年月
				var year = d[0];
				var month  = d[1];
				
			//	var ddd=getValuePairBySql('wage_is_submit_state',"<year>"+year+"</year><month>"+SalMonth.value+"</month>");//是否有提交数据 1有0无
			//	var is_submit_state = ddd[0];
				var dd=getValuePairBySql('wage_is_check',"<year>"+SalYear.value+"</year><month>"+SalMonth.value+"</month>");//是否审核 1审核0未审核
				var is_check = dd[0];
				var ss=getValuePairBySql('wage_is_slry_num',"<userid>"+getUserID()+"</userid><year>"+SalYear.value+"</year><month>"+SalMonth.value+"</month>");//是否有考勤数据 =0 无 >0 有
				var is_wage_num = ss[0];
				var isSubmitFlag = getValuePairBySql("hr_attwageDataCheckInit_isAccount","<salyear>"+SalYear.value+"</salyear><salmonth>"+SalMonth.value+"</salmonth>");//是否结账
				var you_num = getValuePairBySql("wage_is_check_wage_wage","<salyear>"+SalYear.value+"</salyear><salmonth>"+SalMonth.value+"</salmonth>");//是否当前年月有考勤数据
				var add_value =  getValuePairBySql("hr_batch_TiaoZheng_start","<userid>"+getUserID()+"</userid>");//是否有增删改的权限
			  var add_value_end = add_value[0];
			  
				if(add_value_end==1 && you_num[0] == 1 && year==SalYear.value && month==SalMonth.value && is_wage_num>0 && is_check==0 && isSubmitFlag[0]==0)
				{
					hr_att_wss_wageBatch_Bauthselect.disabled=false;
				}
				else
				{
					hr_att_wss_wageBatch_Bauthselect.disabled=true;
				}
			
	}
function changeSalMonthPurview() {// 月份更改后判断按钮权限
	getPurviewSelect();
	getPurviewbutton();
	SalYear1.value = SalYear.value;
	SalMonth1.value = SalMonth.value;
	hr_att_wss_wageBatch_select.click();
}

function loadData(isLeaf, pk, parameter, label) {// 点击数节点时触发，用于加载右侧的列表数据

	zt_ref = '1';// 点树时为1
	var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
	xmlDoc.async = "false";
	xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + pk + "</root>");
	// 从PK中取得dcode sign
	dept_code.value = xmlDoc.getElementsByTagName("dcode")[0].firstChild.nodeValue;
	sign.value = xmlDoc.getElementsByTagName("sign")[0].firstChild.nodeValue;
	inlower.value = xmlDoc.getElementsByTagName("inlower")[0].firstChild.nodeValue;
	userid.value = getUserID();
	// SalSort1.value = SalSort.value;	
	SalYear1.value = SalYear.value;
	SalMonth1.value = SalMonth.value;
	if(SalYear1.value ==""||SalMonth1.value==""){
		alert("请选择年度及月份信息！");
		return false;
	}
	
	resultTable.style.display = "";
	
	hr_att_wss_wageBatch_select.click();
	
}
//王楠添加 通过年月判断是否已经结账，从而判断"月初始化"、"批量调整"按钮是否可用
function judgeAccountData(){
	if(SalYear.value==""){
		alert("请选择年度！");
		return false;
	}
	if(SalMonth.value==""){
		alert("请选择月份！");
		return false;
	}
	
  	var isSubmitFlag = getValuePairBySql("hr_attwageDataCheckInit_isAccount","<salyear>"+SalYear.value+"</salyear><salmonth>"+SalMonth.value+"</salmonth>");
  	if(isSubmitFlag[0]=="1"){
  		hr_slrSalPersonnelPersonnelSal_CreateSal.disabled = "disabled";
  		hr_att_wss_wageBatch_Bauthselect.disabled = "disabled";
  	}else{
  		hr_slrSalPersonnelPersonnelSal_CreateSal.disabled = "";
  		hr_att_wss_wageBatch_Bauthselect.disabled = "";
  	}
}

function show_isstop(t) {
	showstop = t.checked ? '1' : '0';
	var pk = getPk();
	unitTree.expand = texpands;
	unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect", pk);
}

function include_lower(t) {
	showlower = t.checked ? '1' : '0';
	var pk = getPk();
	unitTree.expand = texpands;
	unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect", pk);
}

function show_all(t) {
	var pk = getPk();
	if (t.checked) {
		texpands = 10;
		unitTree.expand = 10;
		unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect", pk);
	} else {
		texpands = 1;
		unitTree.expand = 1;
		unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect", pk);
	}
}

function getPk() {
	return "<comp_code>" + getCompCode() + "</comp_code>" + "<user_id>" + getUserID() 
			+ "</user_id>" + "<show_duty>0</show_duty>" + "<show_stop>" + showstop 
			+ "</show_stop>" + "<include_lower>" + showlower + "</include_lower>" 
			+ "<is_super>0</is_super>" + "<show_count>0</show_count>";
}

// 通过判断职工的人数来确定下拉列表显示不显示
function disTypeByCount() {
	if (result.getRows() > 0) {
	} else {
		checkFlag = -1;
		selectFlag = true;
	}
	tableType.refresh();
	noDisplayBaseButton();
	// result2.innerHTML="";
	result2.style.display = "none"; // 隐藏result2
}

// 判断result复选框 互斥
var checkFlag = -1; // 记录第一次选择的复选框的行号
var selectFlag = true; // 判断是否需要查询
function judgeCheck() {
	var count = 0;
	var data = result.getElementsByTagName('tbody')[1];
	var trs = data.getElementsByTagName("tr");
	for ( var i = 0; i < trs.length; i++) {
		var inputs = trs[i].getElementsByTagName("input");
		for ( var j = 0; j < inputs.length; j++) {
			if (inputs[j].type == 'checkbox' && inputs[j].checked) {
				count++;
				if (checkFlag == -1) {
					checkFlag = i;
					selectFlag = true;
				} else {
					if (checkFlag != i) {
						result.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag].getElementsByTagName("input")[0].checked = false;
						checkFlag = i;
						selectFlag = true;
					} else {
						selectFlag = false;
					}
				}
			}
		}
	}
	// 恢复复选框行号记录初始值
	if (count == 0) {
		checkFlag = -1; // 恢复复选框行号记录初始值。
		noDisplayBaseButton(); // 如果没选择职工，按钮不显示
	}
}
// 当点击职工信息列表时判断条件以便产生附属信息记录
function getSubInfoList(selectChange) {
	if (result.getRows() == 0) {
		checkFlag = -1; // 如果result记录数为零，恢复复选框行号记录初始值。
	} else {
		judgeCheck();
		var empidTemp = "";
		var tableidTemp = tableType.value;
		// 判断选择下拉框中是否有值
		if (tableidTemp == null || tableidTemp.length == 0) {
			result2.innerHTML = ""; // 清空result2
			noDisplayBaseButton(); // 如果下拉框为空，按钮不显示
			return false;
		}

		if (!selectFlag && selectChange == "")
			return false; // 如果result选择未发生改变并且下拉框也未改变就不查询数据库
		var count = 0;
		var data = result.getElementsByTagName('tbody')[1];
		var trs = data.getElementsByTagName("tr");
		for ( var i = 0; i < trs.length; i++) {
			var inputs = trs[i].getElementsByTagName("input");
			for ( var j = 0; j < inputs.length; j++) {
				if (inputs[j].type == 'checkbox' && inputs[j].checked) {
					// 得到选择的职工empid
					var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
					xmlDoc.async = "false";
					xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + inputs[j].value + "</root>");
					empidTemp = xmlDoc.getElementsByTagName("empid")[0].firstChild.nodeValue;
					count++;
				}
			}
		}
		// 判断是否选择职工列表中的记录
		if (count == 0) {
			alert('请选择一条职工记录!');
			result2.innerHTML = "";
			noDisplayBaseButton(); // 如果没有选择职工记录，按钮不显示
			return false;
		}
		if (count > 1) {
			alert('只能选择一条职工记录!');
			result2.innerHTML = "";
			return false;
		}
		subInfoUserid.value = getUserID();
		subInfoEmpid.value = empidTemp;
		subInfoTableid.value = tableidTemp;
		hr_att_wss_wageBatch_select.click();
		result2.style.display = ""; // 显示result2
	}
}

// 按钮权限查询
function getPurview() {
	var acctyear = '';
	acctyear = getAcctYear();
	if (acctyear == '') {
		addPermButton.style.display = "none";
		updatePermButton.style.display = "none";
		deletePermButton.style.display = "none";
		changeSalPermButton.style.display = "none";
		salPermButton.style.display = "none";
	} else {
		window.xmlhttp.post('hr_slrSalchangeChangeMaintenance_buttonPurview', "<tableid>" + tableType.value + "</tableid><userid>" + getUserID() + "</userid><empid>" + subInfoEmpid.value + "</empid>");
		var resText = window.xmlhttp._object.responseXml;

		var addPerm = resText.getElementsByTagName("td")[0].firstChild.nodeValue;
		var editPerm = resText.getElementsByTagName("td")[1].firstChild.nodeValue;
		var deletePerm = resText.getElementsByTagName("td")[2].firstChild.nodeValue;
		var salPerm = resText.getElementsByTagName("td")[3].firstChild.nodeValue;
		var evaluatePerm = resText.getElementsByTagName("td")[4].firstChild.nodeValue;
		var temp = resText.getElementsByTagName("td")[5].firstChild.nodeValue;
		temp = 1;
		if (temp == 0) {
			addPermButton.style.display = "none";
			updatePermButton.style.display = "none";
			deletePermButton.style.display = "none";
			changeSalPermButton.style.display = "none";
			salPermButton.style.display = "none";
		} else {
			if (addPerm != '0') {
				addPermButton.style.display = "";
			} else {
				addPermButton.style.display = "none";
			}
			if (editPerm != '0' && result2.getRows() > 0) {
				updatePermButton.style.display = "";
			} else {
				updatePermButton.style.display = "none";
			}
			if (deletePerm != '0' && result2.getRows() > 0) {
				deletePermButton.style.display = "";
			} else {
				deletePermButton.style.display = "none";
			}
			salPerm = '0';
			if (salPerm != '0' && tableType.value == 'hr_slr_change') {
				// if(salPerm!='0'){
				changeSalPermButton.style.display = "";
				salPermButton.style.display = "";
			}
			// evaluatePerm = '1';
			// alert(evaluatePerm+tableType.value);
			if (evaluatePerm != '0' && tableType.value == 'hr_emp_evaluate') {
				addPermButton.style.display = "";
			}
		}
	}
}

// 判断result2复选框 互斥
var checkFlag2 = -1; // 记录第一次选择的复选框的行号
function judgeCheck2() {
	if (result2.getRows() == 0) {
		checkFlag2 = -1; // 如果result2记录数为零，恢复复选框行号记录初始值。
	} else {
		var count = 0;
		var data = result2.getElementsByTagName('tbody')[1];
		var trs = data.getElementsByTagName("tr");
		for ( var i = 0; i < trs.length; i++) {
			var inputs = trs[i].getElementsByTagName("input");
			for ( var j = 0; j < inputs.length; j++) {
				if (inputs[j].type == 'checkbox' && inputs[j].checked) {
					count++;
					if (checkFlag2 == -1) {
						checkFlag2 = i;
					} else {
						if (checkFlag2 != i) {
							result2.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag2].getElementsByTagName("input")[0].checked = false;
							checkFlag2 = i;
						}

					}
				}
			}
		}
		if (count == 0)
			checkFlag2 = -1; // 恢复复选框行号记录初始值
	}
}

// 添加
function insert() {
	ridvalue = "";
	openIFDialog(window, 'insert.html', 'dialogWidth:520px;dialogHeight:700px')
}
// 添加页面需要的参数
function insertMess(id) {
	this.pk = {
		id : 'rid',
		value : id + "##" + subInfoTableid.value
	};
	this.button = {
		text : '保存',
		name : 'hr_slrSalchangeChangeMaintenance_insert',
		onclick : 'if(editTable.submit(this)){closePage();window.parentPage.refreshParent();}',
		accessKey : 'S'
	};
}
// 添加页面需要的参数
function insertCallbacks() {
	setInsertPage('hr_slrSalchangeChangeMaintenance_showFields', '<tableid>' + tableType.value + '</tableid><userid>' + getUserID() + '</userid>', '', "<rid>" + ridvalue + "</rid><userid>" + getUserID() + "</userid>", new insertMess(ridvalue));
}

// 修改
function update() {
	var count = 0;
	var data = result2.getElementsByTagName('tbody')[1];
	var trs = data.getElementsByTagName("tr");
	for ( var i = 0; i < trs.length; i++) {
		var inputs = trs[i].getElementsByTagName("input");
		for ( var j = 0; j < inputs.length; j++) {
			if (inputs[j].type == 'checkbox' && inputs[j].checked) {
				// 得到要修改的记录的rid
				var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
				xmlDoc.async = "false";
				xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + inputs[j].value + "</root>");
				ridvalue = xmlDoc.getElementsByTagName("rid")[0].firstChild.nodeValue;
				count++;
			}
		}
	}
	if (count == 0) {
		alert('请选择要修改的记录!');
		return false;
	}
	if (count > 1) {
		alert('只能选择一条记录进行修改!');
		return false;
	}

	openDialog('update.html', 'dialogWidth:520px;dialogHeight:700px')
}
// 修改页面需要的参数
function updateMess(id) {
	this.pk = {
		id : 'rid',
		value : id
	};
	this.button = {
		text : '保存',
		name : 'hr_slrSalchangeChangeMaintenance_update',
		onclick : 'if(editTable.submit(this)){closePage();window.parentPage.refreshParent();}',
		accessKey : 'S'
	};
}
// 修改页面需要的参数
function updateCallbacks() {
	setUpdatePage('hr_slrSalchangeChangeMaintenance_update_list', '<tableid>' + tableType.value + '</tableid><userid>' + getUserID() + '</userid>', 'hr_slrSalchangeChangeMaintenance_update', "<tableid>" + tableType.value + "</tableid><rid>" + ridvalue + "</rid><userid>" + getUserID() + "</userid>", new updateMess(ridvalue));
}

// 刷新结果集
function refreshParent() {
	window.document.body.all['hr_att_wss_wageBatch_select'].click();
}

// 删除
function deleteItems(obj) {
	var count = 0;
	var data = result2.getElementsByTagName('tbody')[1];
	var trs = data.getElementsByTagName("tr");
	for ( var i = 0; i < trs.length; i++) {
		var inputs = trs[i].getElementsByTagName("input");
		for ( var j = 0; j < inputs.length; j++) {
			if (inputs[j].type == 'checkbox' && inputs[j].checked) {
				count++;
			}
		}
	}
	if (count == 0) {
		alert('请选择要删除的记录!');
		return false;
	}
	if (confirm("确认要删除选中的记录吗？")) {
		result2.submit_choose(obj.name, "<userid>" + getUserID() + "</userid><tableid>" + tableType.value + "</tableid>");
		result2.refresh();
	}

}

// 调整津补贴
function changeSal() {
	openDialog("changeSal.html?load=<empid>" + subInfoEmpid.value + "</empid>", 'dialogWidth:700px;dialogHeight:520px');
}

// 核定工资
function checkSal() {
	window.xmlhttp.post('hr_slrSalchangeChangeMaintenance_checksal', "<userid>" + getUserID() + "</userid><empid>" + subInfoEmpid.value + "</empid>");
	var resText = window.xmlhttp._object.responseXml;
	var temp = resText.xml;
	if (temp.indexOf('操作成功') > 0) {
		alert('操作成功');
	} else {
		alert('操作失败');
	}
}

// 不显示按钮
function noDisplayBaseButton() {
	addPermButton.style.display = "none";
	updatePermButton.style.display = "none";
	deletePermButton.style.display = "none";
	changeSalPermButton.style.display = "none";
	salPermButton.style.display = "none";
}

// 科室职位目录按钮
function deptDutyList() {
	zt_ref = '1';
	unitTree.style.display = '';
	tree_param.style.display="";
	//tree_param_iower.style.display="";
	condTable.style.display = 'none';
	// 清空页面上的相关信息
	clearRelationInfo();
}

// 清空页面上的相关信息
function clearRelationInfo() {

	// result.innerHTML = ""; //清空职工列表


	checkFlag = -1; // 初始化result的选择初始值

	selectFlag = true; // 初始化 判断是否需要查询变量
	// checkFlag2=-1; //初始化result2的选择初始值
	// noDisplayBaseButton(); //不显示附属信息功能按钮
	// result2.style.display="none"; //隐藏result2
}

// 条件查询按钮
function condSelect() {
	zt_ref = '0';

	unitTree.style.display = 'none';
	tree_param.style.display="none";

	condTable.style.display = '';
	commonCondDispaly.value = '';
	// 清空页面上的相关信息

	clearRelationInfo();

	commonCondSelectByUserid.value = getUserID();
	hr_att_wss_wageCycleBatch_commonCondSelect.click();
}

// 判断commonCondList复选框 互斥 给条件显示框赋值具体条件
var commonCondListCheckFlag = -1; // 记录第一次选择的复选框的行号
function judgeCommonCondListCheck() {

	zt_ref = '0';
	if (commonCondList.getRows() > 0) {
		var count = 0;
		var data = commonCondList.getElementsByTagName('tbody')[1];
		var trs = data.getElementsByTagName("tr");
		for ( var i = 0; i < trs.length; i++) {
			var inputs = trs[i].getElementsByTagName("input");
			for ( var j = 0; j < inputs.length; j++) {
				if (inputs[j].type == 'checkbox' && inputs[j].checked) {
					count++;
					if (commonCondListCheckFlag == -1) {
						commonCondListCheckFlag = i;
					} else {
						if (commonCondListCheckFlag != i) {
							commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked = false;
							commonCondListCheckFlag = i;
						}
					}
				}
			}
		}
		if (count == 0) {
			commonCondListCheckFlag = -1; // 恢复复选框行号记录初始值
			commonCondDispaly.value = ""; // 清空条件显示框
			// 清空页面上的相关信息
			clearRelationInfo();
		} else {
		
			if(commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild==null){
				return;
			}		
			// 给条件显示框赋值具体条件
			commonCondDispaly.value = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
			// 按常用条件查询职工
			whereList = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
			whereSelectWss = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
			
			
			// 条件说明cond_desc 条件列表cond_list 条件内容cond
			cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
			cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
			cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
			
			//处理特殊字符 "<"、">" 变成 "&lt;"、"&gt;"  
				cond = cond.replace(/</g,"&lt;");
				cond = cond.replace(/>/g,"&gt;");
				
			WhereSelect();
			/*
			 * var userid_cond = getUserID(); var common_cond =
			 * commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
			 * var common_cond_list =
			 * commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
			 * 
			 * 
			 * window.xmlhttp.post('hr_slrSalPersonnelPersonnelSal_findEmpByCommonCond', "<userid_cond>"+userid_cond+"</userid_cond><common_cond>"+common_cond+"</common_cond><common_cond_list>"+common_cond_list+"</common_cond_list><salsort>"+SalSort.value+"</salsort><salyear>"+SalYear.value+"</salyear><salmonth>"+SalMonth.value+"</salmonth>");
			 * var resText = window.xmlhttp._object.responseXml;
			 * result.setDataXml(resText.xml);
			 */
			// disTypeByCount(); //通过判断职工的人数来确定下拉列表显示不显示
		}
	}
}

// 新条件 在页面顶定义三个全局变量 条件说明cond_desc 条件列表cond_list 条件内容cond
function newCond() {
	zt_ref = '0';
	// 把全局变量清空
	cond_desc = "";
	cond_list = "";
	cond = "";
	// 清空条件显示框
	commonCondDispaly.value = "";
	// 清空页面上的相关信息
	clearRelationInfo();
	if (commonCondListCheckFlag != -1) { // 判断是否选择了常用条件
		// 把列表commonCondList的复选框选择去掉
		commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked = false;
		// 恢复复选框行号记录初始值
		commonCondListCheckFlag = -1;

	}
	conddiag=openIFDialog(window, 'cond.html?load=<cond_type>new</cond_type>', 'dialogWidth:520px;dialogHeight:500px');
}

// 修改条件
function updateCond() {
	if (commonCondDispaly.value.length == 0) {
		alert("条件为空，请选择条件！！！");
		return false;
	} else {
	/*	if (commonCondListCheckFlag == -1) {
			alert("请选择一个常用条件！！！");
			return false;
		} else {*/
			// 条件说明cond_desc 条件列表cond_list 条件内容cond
			//cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
			//cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
			//cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
			// 把列表commonCondList的复选框选择去掉
			//commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked = false;
			// 恢复复选框行号记录初始值
		//	commonCondListCheckFlag = -1;
			// 清空条件显示框
			//commonCondDispaly.value = "";
			// 清空页面上的相关信息
			//clearRelationInfo();
			
			//处理特殊字符 "<"、">" 变成 "小于"、"大于"  
			cond = cond.replace(/</g,"小于");
			cond = cond.replace(/>/g,"大于");
			//处理特殊字符 "&lt;"、"&gt;" 变成 "小于"、"大于"  
				cond = cond.replace(/&lt;/g,"小于");
				cond = cond.replace(/&gt;/g,"大于");

			conddiag=openIFDialog(window, 'cond.html?load=<cond_type>update</cond_type><cond_desc>' + cond_desc + '</cond_desc><cond_list>' + cond_list + '</cond_list><cond>' + cond + '</cond>', 'dialogWidth:520px;dialogHeight:500px');
		//}
	}
}

// 添加新条件后判断按钮的显示
function getPurviewAfterInsertCond() {
	disTypeByCount();
}
 //保存条件 输入条件名称
	  function saveCond(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请新建条件！！！");
	  		return false;
	  	}else{
	  		openIFDialog(window,'../../../../hr/condName.html','dialogWidth:400px;dialogHeight:200px');
	  	}
	  }

// 保存条件
function saveCond1() {
	if (commonCondDispaly.value.length == 0) {
		alert("条件为空，请新建条件！！！");
		return false;
	} else {
		//var cond_title = window.prompt("请输入输入常用条件名称！！！", "");
		if (cond_title == null || cond_title.length == 0) {
			alert("条件名称为空！！！");
			return false;
		} else {
			/*if (cond_desc==null || cond_desc==""){
				cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;			
			}
			if(cond_list==null || cond_list==""){
				cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
			}
			if(cond==null || cond==""){
				cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;		
				//处理特殊字符 "<"、">" 变成 "&lt;"、"&gt;"  		
				cond = cond.replace(new RegExp("<","gm"),"&lt;");
				cond = cond.replace(new RegExp(">","gm"),"&gt;");
			}*/		
			
			//处理特殊字符 "<"、">" 变成 "&lt;"、"&gt;"  		
			cond = cond.replace(new RegExp("<","gm"),"&lt;");
			cond = cond.replace(new RegExp(">","gm"),"&gt;");
			
			// getUserID() 条件名称cond_title 条件说明cond_desc 条件列表cond_list 条件内容cond
			window.xmlhttp.post('hr_att_wss_SalPersonnelPersonnelSal_insertCond', "<user_id>" + getUserID() + "</user_id><cond_title>" + cond_title + "</cond_title><cond_desc>" + cond_desc + "</cond_desc><cond_list>" + cond_list + "</cond_list><cond>" + cond + "</cond>","?isCheck=false");
			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
			// 恢复复选框行号记录初始值
			commonCondListCheckFlag = -1;
			// 清空条件显示框
			commonCondDispaly.value = "";
			// 清空页面上的相关信息
			clearRelationInfo();
			// 刷新常用条件
			hr_att_wss_wageCycleBatch_commonCondSelect.click();
			
			//result.style.display="none";
		}
	}
}

// 删除条件
function deleteCond() {
	if (commonCondListCheckFlag == -1) {
		alert("请选择一个常用条件！！！");
		return false;
	} else {
		if (confirm('确定删除该常用条件吗？')) {
			var cond_id = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[2].firstChild.nodeValue;

			window.xmlhttp.post('hr_att_wss_SalPersonnelPersonnelSal_deleteCond', "<cond_id>" + cond_id + "</cond_id>","?isCheck=false");
			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
			// 恢复复选框行号记录初始值
			commonCondListCheckFlag = -1;
			// 清空条件显示框
			commonCondDispaly.value = "";
			// 清空页面上的相关信息
			clearRelationInfo();
			// 刷新常用条件
			hr_att_wss_wageCycleBatch_commonCondSelect.click();
		} else {
			return false;
		}
	}
}

function getPurviewbutton() {
	var acctyear = '';
	acctyear = getAcctYear();

	window.xmlhttp.post('hr_att_wss_wageBatch_buttonPurview', "<SalYear>" + SalYear.value + "</SalYear><SalMonth>" + SalMonth.value + "</SalMonth>");

	var resText = window.xmlhttp._object.responseXml;

	var hrflag = resText.getElementsByTagName("td")[0].firstChild.nodeValue;

	var cur_year = resText.getElementsByTagName("td")[1].firstChild.nodeValue;

	var cur_month = resText.getElementsByTagName("td")[2].firstChild.nodeValue;

	var salyear = SalYear.value;

	var salmonth = SalMonth.value;
	// if @cur_year is null, 表明系统登录时间不是人事系统 的当前年度
	if (acctyear == '' && hrflag == 'true' && cur_year != salyear && cur_month != salmonth) {
		createSal.disabled = "true";
		perchange.disabled = "true";
		wss_listpurview = "0";
		// changeAll.disabled="true";
		// salsale.disabled="true";
		// document.getElementById("createSal").disabled=true;
		// document.getElementById("perchange").disabled=true;
		// document.getElementById("changeAll").disabled=true;
		// document.getElementById("salsale").disabled=true;
	}
}
function createSal() {// 月初始化
	var salyear = SalYear.value;
	var salmonth = SalMonth.value;
	
	/*var dd=getValuePairBySql('wage_is_check_wage',"<year>"+salyear+"</year><month>"+salmonth+"</month>");
					var is_check_wage = dd[0];
					if(is_check_wage==1)
					{
						hr_slrSalPersonnelPersonnelSal_CreateSal.disabled=true;
					}
					else
						{
							hr_slrSalPersonnelPersonnelSal_CreateSal.disabled=false;
							}*/
	window.xmlhttp.post('hr_att_wss_wageBatch_CreateSal', "<userid>" + getUserID() + "</userid><salyear>" + salyear + "</salyear><salmonth>" + salmonth + "</salmonth>");
	doMsg(window.xmlhttp._object.responseText);
isCheckWage();
isCheckWage_batch();//判断批量调整按钮是否可用
}
function PerAdjust() {

	subBody.load="hr_att_wss_wageCheckInitData";
	subBody.para="<acct_month>"+SalMonth.value+"</acct_month><acct_year>"+SalYear.value+"</acct_year>";
	subBody.hideMsg=true;
	subBody.post();
	var rights=subBody.getHiddenVs();
	/*if(rights[0][0]==0)
	{
		alert("请在考勤数据管理中初始化数据！");
		return;
	}*/
	
	var salyear = SalYear.value;
	var salmonth = SalMonth.value;
  	window.xmlhttp.post('hr_att_wss_wageBatch_checkInit',"<userid>" + getUserID() + "</userid><salyear>" + salyear + "</salyear><salmonth>" + salmonth + "</salmonth>","?isCheck=false");
	var resText = window.xmlhttp._object.responseXml;
	if(resText.getElementsByTagName("td")[0].firstChild.nodeValue<1)
	{
		alert("您还没有进行月初始化!");
		return;
	}
	openIFDialog(window, 'baseInfoMain.html', 'dialogWidth:800px;dialogHeight:600px')
}

function SalAccount() {
	// var salsort = window.parentPage.document.body.all['SalSort'].value;
	// window.xmlhttp.post('hr_slrSalPersonnelPersonnelSal_SalAccount',"<userid>"+getUserID()+"</userid><salsort>"+SalSort.value+"</salsort>");

	subBody.load = 'hr_slrSalPersonnelPersonnelSal_SalAccount';
	subBody.para = "<userid>" + getUserID() + "</userid><salsort>" + SalSort.value + "</salsort>";
	subBody.post();
	var ret = subBody.getHiddenVs();// 以二维数组的形式返回结果
	// alert (ret[0][0]);
	subBody.load = 'hr_slrSalPersonnelPersonnelSal_SalAccount1';
	subBody.para = "<user_id>" + getUserID() + "</user_id><kind_id>" + SalSort.value + "</kind_id><table_id>hr_slr_pay</table_id><comp_code>" + getCompCode() + "</comp_code><copy_code>" + getCopyCode() + "</copy_code><acct_year>" + ret[0][0] + "</acct_year><acct_month>" + ret[0][1] + "</acct_month><localCond>0</localCond><localCondList>0</localCondList>";
	subBody.hideMsg = false;
	subBody.post();
	var res = subBody.getHiddenVs();

	if (res.length > 0) {
		if (res[0][0] == "") {
			alert("计算完成！");
		} else {
			alert(res[0][0]);
		}
	}

}
function BatchChange() {
	openIFDialog(window, 'batchupdate.html', 'dialogWidth:800px;dialogHeight:700px')
	// openDialog("batchupdate.html?load=<salsort>"+SalSort.value+"</salsort><salyear>"+SalYear.value+"</salyear>","dialogWidth:650px;dialogHeight:500px;");
}
function updateOne(obj) {
	var count = 0;
	alert(result2.getElementsByTagName('tbody').length);
	var data = result2.getElementsByTagName('tbody')[1];
	var trs = data.getElementsByTagName("tr");
	// alert('aaaa');
	for ( var i = 0; i < trs.length; i++) {
		var inputs = trs[i].getElementsByTagName("input");
		for ( var j = 0; j < inputs.length; j++) {
			if (inputs[j].type == 'checkbox' && inputs[j].checked) {
				// 得到要修改的记录的empid
				var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
				xmlDoc.async = "false";
				xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + inputs[j].value + "</root>");
				empidvalue = xmlDoc.getElementsByTagName("field_id")[0].firstChild.nodeValue;
				count++;
			}
		}
	}
	/*
	 * var data = result2.getElementsByTagName('tbody')[1]; var trs =
	 * data.getElementsByTagName("tr"); for (var i=0; i<trs.length; i++){ var
	 * inputs = trs[i].getElementsByTagName("input"); for (var j=0; j<inputs.length;
	 * j++) { if (inputs[j].type == 'checkbox' && inputs[j].checked) { count++; } } }
	 */

	if (count == 0) {
		alert('请选择要修改的记录!');
		return false;
	}
	if (count > 1) {
		alert('只能选择一条记录进行修改!');
		return false;
	}

	openDialog('updateOne.html', 'dialogWidth:520px;dialogHeight:500px')
}

// 修改页面需要的参数
function updateCallbacks() {
	setUpdatePage('hr_empDocumentBasicInfoMain_update_list', '<userid>' + getUserID() + '</userid>', 'hr_empDocumentBasicInfoMain_update', "<empid>" + empidvalue + "</empid><userid>" + getUserID() + "</userid>", new updateMess(empidvalue));
}
function WhereSelect() {
	var userid_cond = getUserID();
	var common_cond,common_cond_list;
	//alert(commonCondListCheckFlag)
	if(commonCondListCheckFlag>-1){
		common_cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
		common_cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
		// 处理特殊字符 "<"、">" 变成 "&lt;"、"&gt;"
		common_cond = common_cond.replace(new RegExp("<", "gm"), "&lt;");
		common_cond = common_cond.replace(new RegExp(">", "gm"), "&gt;");
		//alert(common_cond)
	}else{
		common_cond = common_cond2.value.replace(new RegExp("<", "gm"), "&lt;");
		common_cond = common_cond.replace(new RegExp(">", "gm"), "&gt;");
		common_cond_list = common_cond_list2.value
	}

	var salyear = SalYear.value;
	var salmonth = SalMonth.value;
	userid_cond2.value = getUserID();
	//common_cond2.value = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
	//common_cond_list2.value = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;

	salyear2.value = SalYear.value;
	salmonth2.value = SalMonth.value;
	resultTable.style.display = "";
	result.HaveFirstSelCol = true;

	result.AllowUserResize = true;
	result.isDataAutoSubmit = false;
	result.HaveTotalRow = false;
	result.AllowAddNewRow = false;
	result.PageSize = 20;

	
	//alert(1);
	window.xmlhttp.post("hr_att_wss_wageBatch_select_where_Title", "<userid>" + userid_cond + "</userid><common_cond>" + common_cond + "</common_cond><common_cond_list>" + common_cond_list + "</common_cond_list><SalYear1>" + salyear + "</SalYear1><SalMonth1>" + salmonth + "</SalMonth1>", '?isCheck=false');
	var resText = window.xmlhttp._object.responseText;
	if (resText.indexOf("<error>") > 0) {

		window.doMsg(resText);
		return false;
	}
	var tds = window.xmlhttp._object.responseXML.getElementsByTagName("td");
	g_editBits = new Array();
	g_editCols = new Array();
	g_editIds = new Array();
	// g_editColumn = new Array();
	g_printParam = {};
	var headers = "", cs;
	// alert(tds.length);
	for ( var i = 0; i < tds.length; i++) {
		cs = tds[i].text.split("|||");
		g_editBits.push(cs[0]);
		g_editCols.push(cs[1]);
		// g_editColumn.push(cs[4]);

		headers += cs[2] + ",";
		g_printParam["thead_2_" + (i + 1)] = cs[2];
		g_editIds.push(cs[3]);
	}
	headers = headers.substr(0, headers.length - 1);
	g_headXml = creatActiveXHeadT2head(headers);
	result.StartInit(true);
	result.SetHeaderFromXML(creatActiveXHeadXML(headers));

	// edit="type=text;align=left;edit=false;width=150;";
	for ( var i = 0; i < g_editBits.length; i++) {

		if (g_editBits[i] == '2') {
			edit = "type=text;align=left;edit=verify;width=80;";
		} else {
			if (g_editIds[i] == '2')// 如果是数值型
			{
				edit = "type=number;edit=false;align=right;width=80;";
			} else {
				edit = "type=text;align=left;edit=flase;width=80;";
			}
		}

		result.SetTableColumn(i + 1, edit);
	}
	result.LockRowCol(1, 4);

	result.EndInit();
	if(commonCondListCheckFlag>-1){
		common_cond = common_cond.replace("&lt;","<");
		common_cond = common_cond.replace("&gt;",">");
		common_cond2.value=common_cond;
		common_cond_list2.value=common_cond_list;
		//alert(common_cond)
		//alert(common_cond_list)
	}
		
	edittable1.submit(hr_att_wss_wageBatch_select_where, result, true);

}

function beginQuery() {
	// g_btnDis=wageEditBtnAutoDisabled(q_datamonth.value,wageDataInput_gen,wage_add,wage_del,wage_patch,wage_import,wageDataCalculate_exec);
	result.HaveFirstSelCol = true;
	result.AllowUserResize = true;
	result.isDataAutoSubmit = false;
	result.HaveTotalRow = false;
	result.AllowAddNewRow = false;
	result.PageSize = 20;

	// result.ShowHttpMsg=true;
	window.xmlhttp.post("hr_att_wss_wageBatch_select_Title", "<dept_code>" + dept_code.value + "</dept_code><sign>" + sign.value + "</sign><inlower>" + inlower.value + "</inlower><userid>" + userid.value + "</userid><SalYear1>" + SalYear1.value + "</SalYear1><SalMonth1>" + SalMonth1.value + "</SalMonth1>", '?isCheck=false');
	var resText = window.xmlhttp._object.responseText;
	if (resText.indexOf("<error>") > 0) {
		window.doMsg(resText);
		return false;
	}
	var tds = window.xmlhttp._object.responseXML.getElementsByTagName("td");
	g_editBits = new Array();
	g_editCols = new Array();
	g_editIds = new Array();
	// g_editColumn = new Array();
	g_printParam = {};
	var headers = "", cs;
	// alert(tds.length);
	for ( var i = 0; i < tds.length; i++) {
		cs = tds[i].text.split("|||");
		g_editBits.push(cs[0]);
		g_editCols.push(cs[1]);
		// g_editColumn.push(cs[4]);

		headers += cs[2] + ",";
		g_printParam["thead_2_" + (i + 1)] = cs[2];
		g_editIds.push(cs[3]);
	}
	headers = headers.substr(0, headers.length - 1);
	g_headXml = creatActiveXHeadT2head(headers);
	result.StartInit(true);
	result.SetHeaderFromXML(creatActiveXHeadXML(headers));

	// edit="type=text;align=left;edit=false;width=150;";
	for ( var i = 0; i < g_editBits.length; i++) {

		if (g_editBits[i] == '2') {
			edit = "type=text;align=left;edit=verify;width=80;";
		} else {
			if (g_editIds[i] == '2')// 如果是数值型
			{
				edit = "type=number;edit=false;align=right;width=80;";
			} else {
				edit = "type=text;align=left;edit=flase;width=80;";
			}
		}

		result.SetTableColumn(i + 1, edit);
	}

	result.LockRowCol(1, 4);

	result.EndInit();
	// g_update_pk="<comp_code>"+getCompCode()+"</comp_code><copy_code>"+getCopyCode()+"</copy_code><acct_year>"+acct_year.value+"</acct_year><acct_month>"+q_datamonth.value+"</acct_month><cols>"+table_column.value+"</cols>";
	// result.SetUpdateSql("hr_slrSalPersonnelPersonnelSal_updateSave","");//回车后执行UPDATE修改数据库表数据。hr_slr_pay表
	
	return editTable.submit(hr_att_wss_wageBatch_select, result, true);
}

// 按钮权限查询
function getPurviewSelect() {

	var tableType = 'slry_time_detail';

	window.xmlhttp.post('hr_att_wss_wageCycleBatch_buttonPurview', "<tableid>" + tableType + "</tableid><userid>" + getUserID() + "</userid>");

	var resText = window.xmlhttp._object.responseXml;

	var addPerm = resText.getElementsByTagName("td")[0].firstChild.nodeValue;
	var editPerm = resText.getElementsByTagName("td")[1].firstChild.nodeValue;
	var deletePerm = resText.getElementsByTagName("td")[2].firstChild.nodeValue;
	if (addPerm == 0) {
		createSal.disabled = "true";
		perchange.disabled = "true";
	}
	if (deletePerm == 0) {
		perchange.disabled = "true";
	}
}