<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<thead>
		  <tr noWrap='true' class='mainHead'>
	      <!--th style=''><input type='checkbox'/></th-->
				<th noWrap="true">日期</th>
				<th noWrap="true">记录号</th>
	      <th noWrap="true">摘要</th>
				<th noWrap="true">本期增加</th>
				<th noWrap="true">本期休假</th>
				<th noWrap="true">期末积假</th>
      </tr>
  	</thead>
  	<tbody>

	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <!--td align='center'  style=''>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td-->
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=2">
								<xsl:if test="substring(../td[ 2 ],1,4) = 'XJJL' ">
							  <td  noWrap='true' >
							  <a tabindex="-1">
							    <xsl:attribute name="href" >
							    	javascript:view('update.html?load=&lt;id&gt;<xsl:value-of select="../td[2]"/>&lt;/id&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							  </xsl:if>
							  
							  <xsl:if test="substring(../td[ 2 ],1,4) = 'XJZJ' ">
							  <td  noWrap='true' >
							  <a tabindex="-1">
							    <xsl:attribute name="href" >
							    	javascript:view1('update1.html?load=&lt;id&gt;<xsl:value-of select="../td[2]"/>&lt;/id&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							  </xsl:if>
							  <xsl:if test="../td[2] = '' ">
							  <td  noWrap='true' >
							  <xsl:value-of select="."/>
							  </td>
							  </xsl:if>
							</xsl:when>
							<xsl:when test="position()=4 or position()=5">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test=" position()=6">
							<xsl:if test="substring(../td[ 6 ],0,4) = '0.00' and substring(../td[ 3 ],1,4) = '本年累计' ">
								<td align='right'><xsl:value-of select="format-number(.,'')"/></td>
							</xsl:if>
							<xsl:if test="substring(../td[ 6 ],1,4) = '0.00' and substring(../td[ 3 ],1,4) != '本年累计' ">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:if> 
							<xsl:if test="substring(../td[ 6 ],1,4) != '0.00' ">
									<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>                  
							</xsl:if>                                                                                      
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


