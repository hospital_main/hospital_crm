function submitData(obj){
	var res=checkInputs();
	//alert(res);
	//return;
	if(res==null||res=="")
		return;
	window.xmlhttp.post(obj.name, res,"")
	var str = window.xmlhttp._object.responseText
	vacation_amount.value = amount_total;
	if (!window.doMsg(str,"")) {
	  return false;
	}
}
function checkInputs(){
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	var res="";
	var tempRate = "",tempYearMonth='',tempKindCode='';
	var amount,apply_code;
	amount_total = 0;
	for(var i=1;i<trs.length;i++){
		  apply_code = trs[i].getAttribute("apply_code");
		  amount = trs[i].getAttribute("value");
      res+="<record>";
      res+=apply_code;
      res+= "<amount>"+amount+"</amount>";  
  		res+="</record>";
  		amount_total += parseFloat(amount);
	}
	return res;
}
function initInputs(){
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	
	for(var i=1;i<trs.length;i++)
		initTrInputs(trs[i]);
}
//原为探测是否有值改动，现改为探测是否是数字
function textChange(obj){
  if(isNaN(obj.value)){
  	alert('休假天数必须是数字！');
  	obj.value='';
  	obj.focus();
  	return false;
  }else if((obj.value-0) > 1 || (obj.value-0) <= 0){
  	alert('休假天数必须是0到1之间的数字！');
  	obj.value='';
  	obj.focus();
  	return false;
  }
  obj.parentNode.parentNode.value = obj.value;
}
function initTrInputs(tr){
	var pv=tr.getAttribute("value");
	var inp="";
	var id;
  id=getJsGuid();
  var temp = tr.cells[1].innerText;
	tr.cells[1].innerHTML ="<input type='text' class='inputDecimal' id='"+id+"' name='"+id+"' value='"+temp+"' maxlength='4' width='100' onChange='textChange(this);'/>";
}
