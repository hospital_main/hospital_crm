<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
		  <tr noWrap='true' class='mainHead'>
				<th noWrap="true">�ݼ�����</th>
				<th noWrap="true">�ݼ�����</th>
      </tr>
  	</thead>
  	<tbody>

	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:attribute name="apply_code" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      		</xsl:attribute>
      		<xsl:attribute name="value" >
                <xsl:value-of select="td[3]"/>
      		</xsl:attribute>
          <xsl:for-each select="td[position() != 1]">
            <xsl:choose>
							<xsl:when test="position()=1">
							  <td  noWrap='true' ><xsl:value-of select="."/>
							  </td>
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>