<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
		  <tr noWrap='true' class='mainHead'>
	      <!--th style=''><input type='checkbox'/></th-->
				<th noWrap="true">职工信息</th>
				<th noWrap="true">所属科室</th>
	      <th noWrap="true">上期剩假</th>
				<th noWrap="true">本期应休</th>
				<th noWrap="true">本期清除</th>
				<th noWrap="true">本期补假</th>
				<th noWrap="true">本期休假</th>
				<th noWrap="true">累计应休</th>
				<th noWrap="true">累计清除</th>
				<th noWrap="true">累计补假</th>
				<th noWrap="true">累计休假</th>
				<th noWrap="true">期末积假</th>
      </tr>
  	</thead>
  	<tbody>

	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <!--td align='center'  style=''>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td-->
          <xsl:for-each select="td">
            <xsl:choose>
							<!--xsl:when test="position()=1">
							  <td  noWrap='true' >
							  <a tabindex="-1">
							    <xsl:attribute name="href" >
							    	javascript:view('update.html?load=&lt;id&gt;<xsl:value-of select="../td[1]"/>&lt;/id&gt;&lt;state&gt;<xsl:value-of select="../td[position()=11]"/>&lt;/state&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when-->
							<xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


