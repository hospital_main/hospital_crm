<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<xsl:variable name="aa" select="count(/root/tbody/tr)"/>
				<xsl:if test="$aa = 0 ">
					<th>期间</th>
					<th>职工编码</th>
					<th>职工姓名</th>
					<th>部门名称</th>
				</xsl:if>
				<xsl:if test="$aa != 0 ">
					<xsl:for-each select="/root/t2head/tr[1]/td">
						<th nowrap="true">
							<xsl:value-of select="."/>
						</th>
					</xsl:for-each>
				</xsl:if>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:variable name="c" select="position()"/>
						<xsl:if test="string-length($c)&gt;0">
							<xsl:if test="substring( /root/t2head/tr[1]/td[$c] ,string-length( /root/t2head/tr[1]/td[$c] ),1)=' '">
								<td align="right">
									<xsl:value-of select="format-number(.,'##,##0.00')"/>
								</td>
							</xsl:if>
							<xsl:if test="substring( /root/t2head/tr[1]/td[$c] ,string-length( /root/t2head/tr[1]/td[$c] ),1)!=' '">
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:if>
						</xsl:if>
						<xsl:if test="string-length($c)=0">
							<td/>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
