	  var showstop=0,showlower=0,texpands=1;
      //全局变量 职工基本信息empidvalue
      var empidvalue="";
      //条件说明cond_desc 条件列表cond_list 条件内容cond
      var cond_desc = "";
      var cond_list = "";
      var cond = "";
      var cur_year="";
      var cur_month="";
      var cond_title = "";
      var p0=0;
      function init(){//初始化左侧树
      	condTable2.style.display="none";
      	
        var pk=getPk();
        unitTree.expand=1;
        unitTree.setSqlData("wageDateSlrentering_treeselect",pk);
        condID.value='';
        cond_listID.value='';
        unitTree.init();
 
      	//获取当前会计年度
    	subBody.load="wageBaseAcctCurYear_select";
	 	subBody.para="";
		subBody.hideMsg=true;
		subBody.post();
		var rights=subBody.getHiddenVs();
		if(rights.length==0)
		{
			alert("无法获取当前年度！");
			return;
		}
		cur_year=rights[0][0];
		cur_month=rights[0][1];
		year_id.setValue(cur_year);
		month_id.setValue(cur_month);
		yearID.value=year_id.value;
  	  	monthID.value=month_id.value;
		changeYear();
			var p=getValuePairBySql("wage_hr_link_select","<a>"+getCompCode()+"</a><b>"+getCopyCode()+"</b>");
				if(p!=null){
					p0=p[0]
					if(p[0]==1){updateBtn.disabled="";}
					if(p[0]==0){updateBtn.disabled="true";}
				}
				
				year_query.value = year_id.value;
        month_query.value = month_id.value;
        
        
      }

      function changeYear()
      {
      	year_query.value = year_id.value;
    	month_id.para="<acct_year>"+year_id.value+"</acct_year>";
    	month_id.refresh();
    	checkBtn();
      }
      
      function changeMonth()
      {
      		month_query.value = month_id.value;
    	  yearID.value=year_id.value;
    	  monthID.value=month_id.value;
    	  checkBtn();
    	  if(condID.value!="")	refresh();
      }
 
      function refresh()
      {
    	  wageDateSlrother_select.click();
      }
      function checkBtn()
      {
  		if(cur_year==year_id.value && cur_month==month_id.value)
		{
  			updateBtn.disabled="";
  			if(p0==0){updateBtn.disabled="true";}
		}
		else
		{
			//updateBtn.disabled="disabled";
		}
      }
      
      function loadData(isLeaf,pk,parameter,label){//点击数节点时触发，用于加载右侧的列表数据
        //document.getElementById("firstNode").style.display='';
        //alert(isLeaf+":"+pk+":"+parameter+":"+label);
        
        var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
		xmlDoc.async="false";  
		xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + pk + "</root>");
		//从PK中取得dcode sign
		var dept_code = xmlDoc.getElementsByTagName("dcode")[0].firstChild.nodeValue;
		var sign = xmlDoc.getElementsByTagName("sign")[0].firstChild.nodeValue;
		var inlower = xmlDoc.getElementsByTagName("inlower")[0].firstChild.nodeValue;
		//判断部门还是职位
		if(sign=="dept")
		{
			//判断是否含下级部门
			if(showlower=="0")
			{
				cond="sys_dept.dept_code='"+dept_code+"'";
				cond_list="sys_dept";
			}
			else
			{
				cond="sys_dept.dept_code like '"+dept_code+"%'";
				cond_list="sys_dept";
			}
			//cond="sys_dept.dept_code like '"+dept_code+"%'";
			//cond_list="sys_dept";
		}
		else
		{
			cond="sys_emp.duty_code='"+dept_code+"'";
		}
		condID.value=cond;
		cond_listID.value=cond_list;
		wageDateSlrother_select.click();
        //editTable.submit(hr_empDocumentBasicInfoMain_select,result);
      }
      
      
      
      function updatewindows()
      {
      	openDialog("../slrother/updatelsry.html",'dialogWidth:320px;dialogHeight:200px');
      	////openIFDialog(window,'../slrother/updatelsry.html','dialogWidth:320px;dialogHeight:200px');
      	
      }
      
      
      function update(emptype,year_sel,month_sel)
      {
    	  if(confirm("系统将其他工资标准调整数据更新到工资数据中，是否继续?"))
    	  {
    		  	/*
    	    	var count=0;
    	      	var temp ='';
    	      	var data = result.getElementsByTagName('tbody')[1];
    	    	var trs = data.getElementsByTagName("tr"); 
    	        var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
    			xmlDoc.async="false";  
    	    	for (var i=0; i<trs.length; i++){
    	    		var inputs =  trs[i].getElementsByTagName("input");
    	    		for (var j=0; j<inputs.length; j++) {   
    			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
    						temp=temp+"<record>"+inputs[j].value;
    						count++;
    						temp=temp+"</record>\n";
    				    }
    				    
    			    }
    	    	}
    	    	//更新全部数据，不需要选择
    	    	//if (count == 0) {
    		    //  alert('请选择要更新的记录!');
    		    //  return false;
    		    //}
    		    */
    		    window.xmlhttp.post('wageDateSlrother_update','<a></a><b>'+emptype+'</b><c>'+year_sel+'</c><d>'+month_sel+'</d>');
    		    ////window.xmlhttp.post('wageDateSlrother_update','<a></a><b>'+emptype+'</b>');
    		    var resText = window.xmlhttp._object.responseText;
    		    doMsg(resText);
    		    window.dialogArguments.wageDateSlrother_select.click();
    	  }
      }
      
      /** KJHS126 会计核算 薪酬发放-工资数据-人力资源接口-其他工资调整  **/
      function update1(emptype,year_sel,month_sel)
      {
    	  if(confirm("系统将其他工资标准调整数据更新到工资数据中，是否继续?"))
    	  {
    		    var list=(emptype.split(','));
		    		for (var i=0 ;i<list.length;i++){
		    			//alert(list[i].substring(1,4))
							window.xmlhttp.post('wageDateSlrother_update','<a></a><b>'+list[i].substring(1,4)+'</b><c>'+year_sel+'</c><d>'+month_sel+'</d>');
		    		}
    		    
    		    ////window.xmlhttp.post('wageDateSlrother_update','<a></a><b>'+emptype+'</b>');
    		    var resText = window.xmlhttp._object.responseText;
    		    doMsg(resText);
    		    window.dialogArguments.wageDateSlrother_select.click();                
    	  }
      }
      
      
      function show_isstop(t){
        showstop=t.checked? '1':'0';
        var pk=getPk();
        unitTree.expand=texpands;
        unitTree.setSqlData("wageDateSlrentering_treeselect",pk);
      }
      
      function include_lower(t){
        showlower=t.checked? '1':'0';
        var pk=getPk();
        unitTree.expand=texpands;
        unitTree.setSqlData("wageDateSlrentering_treeselect",pk);
      }
      
      function show_all(t){
        var pk=getPk();
        if(t.checked){
          texpands=10;
          unitTree.expand=10;
          unitTree.setSqlData("wageDateSlrentering_treeselect",pk);
        }else{
          texpands=1;
          unitTree.expand=1;
          unitTree.setSqlData("wageDateSlrentering_treeselect",pk);
        }
      }
      
      function getPk(){
        return "<comp_code>"+getCompCode()+"</comp_code>"+"<user_id></user_id>"
               +"<show_duty>1</show_duty>"+"<show_stop>"+showstop+"</show_stop>"
               +"<include_lower>"+showlower+"</include_lower>"+"<is_super>0</is_super>"
               +"<show_count>0</show_count>";
      }
      
      
      //判断result复选框 互斥
      var checkFlag=-1;    //记录第一次选择的复选框的行号
	  function judgeCheck(){
	  	if(result.getRows()>0){
		  	var count = 0;
	      	var data = result.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr");
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	count++;
			        	if(checkFlag==-1){
			        		checkFlag=i;
			        	}else{
			        		if(checkFlag!=i){
						    	result.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag].getElementsByTagName("input")[0].checked=false;
			        			checkFlag=i;
			        		}
			        	}
			        }
		    	}
	    	}
	    	if(count==0) checkFlag=-1;  //恢复复选框行号记录初始值
	    }
	  }
	  
	
	  
	  //科室职位目录按钮
	  function deptDutyList(){
	  	unitTree.style.display='';
	  	treeTable.style.display='';
	  	condTable.style.display='none';
	  	condTable2.style.display='none';
	  	commonCondDispaly.value='';
	  	treeCheckBox.style.display="";
	  	//清空职工列表
	  	result.innerHTML = "";
	  }
	  
	  //条件查询按钮 
	  function condSelect(){
	  	unitTree.style.display='none';
	  	condTable2.style.display='none';
	  	condTable.style.display='';
	  	commonCondSelectByUserid.value = getUserID();
	  	treeCheckBox.style.display="none";
        //清空职工列表
        // result.innerHTML = "";
	  	wageDateSlrentering_commonCondSelect.click();
	  }
	  
	  //判断commonCondList复选框 互斥 给条件显示框赋值具体条件
      var commonCondListCheckFlag=-1;    //记录第一次选择的复选框的行号
	  function judgeCommonCondListCheck(){
	  	if(commonCondList.getRows()>0){
		  	var count = 0;
	      	var data = commonCondList.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr");
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	count++;
			        	if(commonCondListCheckFlag==-1){
			        		commonCondListCheckFlag=i;
			        	}else{
			        		if(commonCondListCheckFlag!=i){
						    	commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
			        			commonCondListCheckFlag=i;
			        		}
			        	}
			        }
		    	}
	    	}
	    	if(count==0){ 
	    		commonCondListCheckFlag=-1;  //恢复复选框行号记录初始值
	    		commonCondDispaly.value = "";  //清空条件显示框
				//清空职工列表
				result.innerHTML = "";
			  	condID.value='';    
		  	    cond_listID.value='';
	    	}else{
	    		//给条件显示框赋值具体条件
				commonCondDispaly.value = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
				//按常用条件查询职工
				var userid_cond = getUserID();
				var common_cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
				var common_cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
				var common_cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
				cond_desc = common_cond_desc;
				cond=common_cond;
				cond_list=common_cond_list;
				condID.value=common_cond;
				cond_listID.value=common_cond_list;
				wageDateSlrother_select.click();
	    	}
	    }
	  }
	  
	  //新条件 在页面顶定义三个全局变量 条件说明cond_desc 条件列表cond_list 条件内容cond
	  function newCond(){
	  		//把全局变量清空
	  		cond_desc = "";
	  		cond_list = "";
	  		cond = "";
	  		//清空条件显示框
			commonCondDispaly.value = "";
			//清空职工列表
			//result.innerHTML = "";
			//判断职工功能按钮的权限
	  		if(commonCondListCheckFlag!=-1){  //判断是否选择了常用条件
		  		//把列表commonCondList的复选框选择去掉
		  		commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
		  		//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1;  
			}
  			openIFDialog(window,'../cond/cond.html?load=<cond_type>new</cond_type>','dialogWidth:520px;dialogHeight:500px');
	  }
	  
	  //修改条件
	  function updateCond(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请选择条件！！！");
	  		return false;
	  	}else{
			cond = cond.replace(/&lt;/g,"小于");
			cond = cond.replace(/&gt;/g,"大于");
			cond = cond.replace(/</g,"小于");
			cond = cond.replace(/>/g,"大于");
		  	openIFDialog(window,'../cond/cond.html?load=<cond_type>update</cond_type><cond_desc>'+cond_desc+'</cond_desc><cond_list>'+cond_list+'</cond_list><cond>'+cond+'</cond>','dialogWidth:520px;dialogHeight:500px');
	  	}
	  }

	  //保存条件 输入条件名称
	  function saveCond(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请新建条件！！！");
	  		return false;
	  	}else{
	  		openIFDialog(window,'../../../../hr/condName.html','dialogWidth:400px;dialogHeight:200px');
	  	}
	  }
	  
	  //保存条件
	  function saveCond1(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请新建条件！！！");
	  		return false;
	  	}else{
  			if(cond_title==null || cond_title.length==0){
  				alert("条件名称为空！！！");
  				return false;
  			}else{
				cond = cond.replace(/</g,"&lt;");
				cond = cond.replace(/>/g,"&gt;");
				cond = cond.replace(/#lt;/g,"&lt;");
				cond = cond.replace(/#gt;/g,"&gt;");
	  			//getUserID() 条件名称cond_title 条件说明cond_desc 条件列表cond_list 条件内容cond
	  			window.xmlhttp.post('wageDateSlrentering_insertCond', "<user_id>"+getUserID()+"</user_id><cond_title>"+cond_title+"</cond_title><cond_desc>"+cond_desc+"</cond_desc><cond_list>"+cond_list+"</cond_list><cond>"+cond+"</cond>");
	  			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
	  			//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1; 
	  			//清空条件显示框
				commonCondDispaly.value = "";
				//清空职工列表
				//result.innerHTML = "";
	  			//刷新常用条件
				wageDateSlrentering_commonCondSelect.click();
	  		}
	  	}
	  }
	  
	  //删除条件
	  function deleteCond(){
	  	if(commonCondListCheckFlag==-1){
	  		alert("请选择一个常用条件！！！");
	  		return false;
	  	}else{
	  		if(confirm('确定删除该常用条件吗？')){
		  		var cond_id = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[2].firstChild.nodeValue;
		  		window.xmlhttp.post('wageDateSlrentering_deleteCond', "<cond_id>"+cond_id+"</cond_id>");
	  			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
	  			//恢复复选框行号记录初始值
			  	commonCondListCheckFlag=-1; 
	  			//清空条件显示框
				commonCondDispaly.value = "";
				//清空职工列表
				result.innerHTML = "";
	  			//刷新常用条件
				wageDateSlrentering_commonCondSelect.click();
	  		}else{
	  			return false;
	  		}
	  	}
	  }
	  
	  
	  
	  
	  
	  function condSelect2(){
			treeCheckBox.style.display='none';
		 	unitTree.style.display='none';
		 	condTable.style.display='none';
		 	condTable2.style.display='';
		 	cond_item_list.para="";
		
		 	cond_item_list.style.display="";
		 	user_id.value = getUserID();
		 	commonCondSelectByUserid.value = getUserID();
		 	//批量更新页面判断通过树还是条件查询职工
		 	//findEmpByTreeOrCond = "cond2";
		   //清空职工列表
		   result.innerHTML = "";
		   
		 	//hr_empDocumentBasicInfoMain_commonCondSelect.click();
 }
	  