<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/wage/data/query/queryView_oneTitle_noSpaceRow.xsl,v 1.1 2012/03/12 01:56:22 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:56:22 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<thead>
				<xsl:variable name="colNum" select="/root/tbody/tr[1]/td[last()]"/>
				<xsl:variable name="totCol" select="count(/root/tbody/tr[1]/td) -1 "/> 
				<tr noWrap='true'>
					<td style="fontsize:maintitle;colspan:colcount"></td>
					<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1 and position() &lt; $colNum + 1 and position() &lt; $totCol + 1 ]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
				</tr>
			</thead>
			
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
				
				<!--pengjin 只显示一行标题 begin -->
				<xsl:if test="position() =1 ">					
				<xsl:variable name="rowNum" select="position()"/>					
					<xsl:for-each select="td[position() &lt; last()]"> 
						<xsl:variable name="c" select="position()"/>
						
						<xsl:if test="$c mod $colNum = 0 or $c=$totCol">					
					
								<tr noWrap='true'>
								<xsl:if test="$c mod $colNum = 0 and $c != $totCol">
									<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'‖')">
												<xsl:value-of select="substring-before(. ,'‖')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'‖'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum = 0 and $c = $totCol">
									<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'‖')">
												<xsl:value-of select="substring-before(. ,'‖')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'‖'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum != 0 and $c = $totCol">
									<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; $c - ($c mod $colNum)  and position() &lt; $c + 1 ]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'‖')">
												<xsl:value-of select="substring-before(. ,'‖')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'‖'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
							</tr>
							
							</xsl:if >
						</xsl:for-each>							
					</xsl:if >
					
				<!--pengjin 只显示一条标题 end -->
				
				<xsl:if test="position() &gt;1 ">
					<xsl:variable name="rowNum" select="position()"/>					
					
					<xsl:for-each select="td[position() &lt; last()]"> 
						<xsl:variable name="c" select="position()"/>
						
						<xsl:if test="$c mod $colNum = 0 or $c=$totCol">
							<!--pengjin 一行数据一行标题 begin -->
							<!--tr noWrap='true'>
								<xsl:if test="$c mod $colNum = 0 and $c != $totCol">
									<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'‖')">
												<xsl:value-of select="substring-before(. ,'‖')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'‖'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum = 0 and $c = $totCol">
									<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'‖')">
												<xsl:value-of select="substring-before(. ,'‖')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'‖'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum != 0 and $c = $totCol">
									<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; $c - ($c mod $colNum)  and position() &lt; $c + 1 ]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'‖')">
												<xsl:value-of select="substring-before(. ,'‖')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'‖'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
							</tr-->
							<!--pengjin 一行数据一行标题 end -->
							
							<tr noWrap='true'>
								<xsl:if test="$c mod $colNum = 0 and $c != $totCol">
									<xsl:for-each select="/root/tbody/tr[$rowNum]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<xsl:variable name="tc" select="$c - $colNum + position() "/>
										<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
											<xsl:if test="substring( /root/tbody/tr[1]/td[$c] ,string-length( /root/tbody/tr[1]/td[$c] ),1)=' '">
												<xsl:attribute name="align">right</xsl:attribute>
												<!--xsl:value-of select="format-number(.,'##,##0.00')"/-->
												<xsl:value-of select="."/>
											</xsl:if>
											<xsl:if test="substring( /root/tbody/tr[1]/td[$c] ,string-length( /root/tbody/tr[1]/td[$c] ),1)!=' '">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum = 0 and $c = $totCol">
									<xsl:for-each select="/root/tbody/tr[$rowNum ]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<xsl:variable name="tc" select="$c - $colNum + position() "/>
										<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
											<xsl:if test="substring( /root/tbody/tr[1]/td[$c] ,string-length( /root/tbody/tr[1]/td[$c] ),1)=' '">
												<xsl:attribute name="align">right</xsl:attribute>
												<!--xsl:value-of select="format-number(.,'##,##0.00')"/-->
												<xsl:value-of select="."/>
											</xsl:if>
											<xsl:if test="substring( /root/tbody/tr[1]/td[$c] ,string-length( /root/tbody/tr[1]/td[$c] ),1)!=' '">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum != 0 and $c = $totCol">
									<xsl:for-each select="/root/tbody/tr[$rowNum ]/td[position() &gt; $c - ($c mod $colNum)  and position() &lt; $c + 1 ]">
										<xsl:variable name="tc" select="$c - ($c mod $colNum) + position() "/>
										<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
											<xsl:if test="substring( /root/tbody/tr[1]/td[$c] ,string-length( /root/tbody/tr[1]/td[$c] ),1)=' '">
												<xsl:attribute name="align">right</xsl:attribute>
												<!--xsl:value-of select="format-number(.,'##,##0.00')"/-->
												<xsl:value-of select="."/>
											</xsl:if>
											<xsl:if test="substring( /root/tbody/tr[1]/td[$c] ,string-length( /root/tbody/tr[1]/td[$c] ),1)!=' '">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
							</tr>
						</xsl:if>
					</xsl:for-each>
					<xsl:if test="/root/annex/annex_emptyrow = 1 ">
						<tr>
							<td height="3" style="pagebreak:deny"><xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute></td>
						</tr>
					</xsl:if>
				</xsl:if>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
