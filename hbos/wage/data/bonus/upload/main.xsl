<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
     <tr noWrap="true" class="mainHead">
      <th><input type="checkbox"/></th>
      <th>年月</th>       
	  <th>部门编号</th>
	  <th>部门名称</th>
	  <th>补贴类型</th>
	  <th>补贴金额</th>
	  <th>状态</th>
      <th>审核人</th>      
      <th>备注</th>        
    </tr>
  </thead>
  <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
        <td align='center'>
          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            <xsl:attribute name="value" >
              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      	    </xsl:attribute>
    	  </input>
          </td>
          <xsl:for-each select="td">
            <td >
              <xsl:choose>
                <xsl:when test="position()=2">
				  <a href="#">
					  <xsl:attribute name="onclick">
						openDetailItems("&lt;year_month&gt;<xsl:value-of select="../td[1]"/>&lt;/year_month&gt;&lt;sys_dept_code&gt;<xsl:value-of select="../td[2]"/>&lt;/sys_dept_code&gt;&lt;item_name&gt;<xsl:value-of select="../td[4]"/>&lt;/item_name&gt;")
					  </xsl:attribute>
					    <xsl:value-of select="../td[2]"/>
				  </a>
                </xsl:when>
		        <xsl:when test="position() = 5">
  		       <xsl:value-of select="format-number(.,'0.00')"/>
  		        </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
  	  </xsl:for-each>
  	</tr>
      </xsl:for-each>  	
    </tbody>
  </xsl:template>
</xsl:stylesheet>
