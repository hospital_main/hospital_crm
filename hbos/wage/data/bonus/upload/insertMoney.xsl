<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
     <tr noWrap="true" class="mainHead">
      <th>职工编号</th>       
	  <th>职工姓名</th>
	  <th>人员性别</th>
	  <th>补贴类型</th>
	  <th>补贴金额</th>
	  <th>状态</th>     
    </tr>
  </thead>
  <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
          <xsl:for-each select="td">
            <td >
                <xsl:value-of select="."/>
            </td>
  	  </xsl:for-each>
  	</tr>
      </xsl:for-each>  	
    </tbody>
  </xsl:template>
</xsl:stylesheet>
