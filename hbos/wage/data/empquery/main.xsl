<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">	
		<thead>
			<xsl:variable name="bodyrows" select="/root/tbody/tr"/>
			<xsl:variable name="aa" select="count(/root/tbody/tr)"/>
			<xsl:variable name="columns" select="count(/root/tbody/tr[1]/td)"/>
			<tr noWrap='true' class='mainHead'>
			
				<xsl:if test="$aa = 0 ">
						<th>职工编码</th>
						<th>职工姓名</th>
						<th>部门名称</th>
				</xsl:if>
				<xsl:if test="$aa != 0 ">
					
						
						
					<xsl:for-each select="/root/t2head/tr">
			    	<xsl:choose>
			    		<xsl:when test="position()=1">
				          <xsl:for-each select="td">
				          	<xsl:variable name="cols" select="position()"/>
				          	<xsl:variable name="cols_name" select="."/>
				          	<xsl:choose>
					          	<xsl:when test=" position()!=last()">
				          			<th noWrap="true">
						            	<xsl:value-of select="."/>
						            	
				          			</th>
					          	</xsl:when>
			              </xsl:choose>
				          </xsl:for-each>
			    		</xsl:when>
			    		
		        </xsl:choose>
	   			</xsl:for-each>
				</xsl:if>
			</tr>
		</thead>                 
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="row_index" select="position()"/>
	    	
	    			<tr>
		          <xsl:for-each select="td">
		          	<xsl:variable name="c" select="position()"/>
		          	<xsl:choose>
		          	
			          	<xsl:when test=" position()!=last() ">
			          		<xsl:if test="string-length($c)&gt;0">
											<xsl:if test="substring( /root/tbody/tr[1]/td[$c] ,string-length( /root/tbody/tr[1]/td[$c] ),1)=' '">
												<td noWrap="true" align="right">
													<xsl:value-of select="format-number(.,'##,##0.00')"/>
												</td>
											</xsl:if>
											<xsl:if test="substring( /root/tbody/tr[1]/td[$c] ,string-length( /root/tbody/tr[1]/td[$c] ),1)!=' '">
												<td noWrap="true">
													<xsl:value-of select="."/>
												</td>
											</xsl:if>
										</xsl:if>
			          		
			          		
			            	
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		          
		  			</tr>
	    	
   		</xsl:for-each>
   		
		</tbody>
	</xsl:template>
</xsl:stylesheet>