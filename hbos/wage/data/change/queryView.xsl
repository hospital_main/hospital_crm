<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap='true' class='mainHead'>
					<td style="fontsize:maintitle;colspan:6"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
			</tr>
			  
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>职工编码</td> 
				<td nowrap='true'>职工姓名</td>
				<td nowrap='true'>人员类别</td> 
				<td nowrap='true'>部门</td>
				<td nowrap='true'>工资</td>
				<td nowrap='true'>变动类型</td>
			</tr>   
		</thead>   
		     
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">   
						
							<xsl:choose>
								<xsl:when test=" position() = 5 ">
								<td align="right">
									<xsl:value-of select="format-number(.,'##,##0.00')"/>
								</td>	
								</xsl:when>
								<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>  
								</td>	
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>

