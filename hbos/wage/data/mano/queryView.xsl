<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/wage/data/mano/queryView.xsl,v 1.1 2012/03/12 01:56:22 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:56:22 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
		<thead>
			<tr>
				<td style="colspan:10;fontsize:maintitle">职工工资调整查询</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr>
				<td  nowrap='true' style='fontsize:coltitle'>职工编码</td>
				<td  nowrap='true' style='fontsize:coltitle'>职工姓名</td>
				<td  nowrap='true' style='fontsize:coltitle'>人员类别</td>
				<td  nowrap='true' style='fontsize:coltitle'>部门</td>
				<td  nowrap='true' style='fontsize:coltitle'>调整前金额</td>
				<td  nowrap='true' style='fontsize:coltitle'>调整比例</td>
				<td  nowrap='true' style='fontsize:coltitle'>调整后金额</td>
				<td  nowrap='true' style='fontsize:coltitle'>调整批次</td>
				<td  nowrap='true' style='fontsize:coltitle'>经办日期</td>
				<td  nowrap='true' style='fontsize:coltitle'>调整说明</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=5 or position()=6 or position()=7">
								<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
