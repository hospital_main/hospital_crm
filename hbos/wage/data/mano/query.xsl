<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th style=''>职工编码</th>
				<th nowrap='true'>职工姓名</th>
				<th nowrap='true'>人员类别</th>
				<th nowrap='true'>部门</th>
				<th nowrap='true'>调整前金额</th>
				<th nowrap='true'>调整比例</th>
				<th nowrap='true'>调整后金额</th>
				<th nowrap='true'>调整批次</th>
				<th nowrap='true'>经办日期</th>
				<th nowrap='true'>调整说明</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=5 or position()=6 or position()=7">
								<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>