<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th>职工编码</th>
      	<th>职工姓名</th>
      	<th>职工部门</th>
      	<th>人员类别</th>
      	<th>公积金账号</th>
      	<th>个人缴费</th>
      	<th>单位缴费</th>
      	<th>总计</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
                    <xsl:choose>
                      <xsl:when test="position()=6 or position()=7 or position()=8">
                        <td align='right'>
                          <xsl:value-of select="format-number(.,'#,##0.00')"/>
                        </td>
                      </xsl:when>
                      <xsl:otherwise>
                        <td align='left'><xsl:value-of select="."/></td>
                      </xsl:otherwise>
                    </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
      	<tr>
      		<td align="center">总计</td>
      		<td></td>
      		<td></td>
      		<td></td>
      		<td></td>
      		<td align="right">
      			<xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),'#,##0.00')"/>
      		</td>
      		<td align="right">
      			<xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/>
      		</td>
       		<td align="right">
      			<xsl:value-of select="format-number(sum(/root/tbody/tr/td[8]),'#,##0.00')"/>
      		</td>
      	</tr>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

