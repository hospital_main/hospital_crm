<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
  	<colgroup>		       
				<col style = 'width:200mm'/>
				<col style = 'width:200mm'/>
				<col style = 'width:200mm'/>
				<col style = 'width:200mm'/>
				<col style = 'width:200mm'/>
				<col style = 'width:200mm'/>
				<col style = 'width:200mm'/>
				<col style = 'width:200mm'/>
		</colgroup>
    <thead>
    	<tr>
					<td style="fontsize:maintitle;colspan:5"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
			</tr>
      <tr noWrap='true' class='mainHead'>
      	<td nowrap='true' style="fontsize:subtitle;">职工编码</td>
      	<td nowrap='true' style="fontsize:subtitle;">职工姓名</td>
      	<td nowrap='true' style="fontsize:subtitle;">职工部门</td>
      	<td nowrap='true' style="fontsize:subtitle;">人员类别</td>
      	<td nowrap='true' style="fontsize:subtitle;">公积金账号</td>
      	<td nowrap='true' style="fontsize:subtitle;">个人缴费</td>
      	<td nowrap='true' style="fontsize:subtitle;">单位缴费</td>
      	<td nowrap='true' style="fontsize:subtitle;">总计</td>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
                    <xsl:choose>
                      <xsl:when test="position()=6 or position()=7 or position()=8">
                        <td align='right'>
                          <xsl:value-of select="format-number(.,'#,##0.00')"/>
                        </td>
                      </xsl:when>
                      <xsl:otherwise>
                        <td align='left'><xsl:value-of select="."/></td>
                      </xsl:otherwise>
                    </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
      	<tr>
      		<td align="center">总计</td>
      		<td></td>
      		<td></td>
      		<td></td>
      		<td></td>
      		<td align="right">
      			<xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),'#,##0.00')"/>
      		</td>
      		<td align="right">
      			<xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/>
      		</td>
       		<td align="right">
      			<xsl:value-of select="format-number(sum(/root/tbody/tr/td[8]),'#,##0.00')"/>
      		</td>
      	</tr>
    </tbody>
   </root>
  </xsl:template>
</xsl:stylesheet>

