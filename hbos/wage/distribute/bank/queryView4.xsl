<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/wage/distribute/bank/queryView4.xsl,v 1.2 2013/09/25 08:47:50 yaoqingsong Exp $
 $Author: yaoqingsong $
 $Date: 2013/09/25 08:47:50 $
 $Revision: 1.2 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<xsl:variable  name="pageCnt" select="/root/t2head/tr[1]/td[last()]"/>
			<thead>
				<tr noWrap='true'>
					<td style="colspan:colcount;fontsize:maintitle"></td>
					<xsl:for-each select="/root/t2head/tr[1]/td[position()&gt;2]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
					
					
					<xsl:for-each select="/root/t2head/tr[1]/td[position()&gt;1]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
					
				</tr>
				<tr noWrap='true'>
					<td style="colspan:colcount;fontsize:maintitle"></td>
					<xsl:for-each select="/root/t2head/tr[1]/td[position()&gt;2]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
					
					
					<xsl:for-each select="/root/t2head/tr[1]/td[position()&gt;1]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
					
				</tr>
				<tr noWrap='true'>
					<xsl:for-each select="/root/t2head/tr[1]/td[position() &lt; last()]">
						<td  nowrap='true' style='fontsize:coltitle'>
							<xsl:if test="contains( . ,'��')">
								<xsl:value-of select="substring-before(. ,'��')"/>
							</xsl:if>
							<xsl:if test="not(contains( . ,'��'))">
								<xsl:value-of select="."/>
							</xsl:if>
						</td>
						
					</xsl:for-each>
					
					
					<xsl:for-each select="/root/t2head/tr[1]/td[position() &lt; last()]">
						<td  nowrap='true' style='fontsize:coltitle'>
							<xsl:if test="contains( . ,'��')">
								<xsl:value-of select="substring-before(. ,'��')"/>
							</xsl:if>
							<xsl:if test="not(contains( . ,'��'))">
								<xsl:value-of select="."/>
							</xsl:if>
						</td>
					</xsl:for-each>
					
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr[position() mod 2 = 1]">
					<xsl:variable name="rownum" select="position()"/>
					<xsl:variable name="vhlastrow" select="last()"/>
					
					<tr>
						<xsl:for-each select="td"> 
							<xsl:variable name="c" select="position()"/>
							<xsl:if test="string-length($c)&gt;0">
								<xsl:if test="contains( /root/t2head/tr[1]/td[$c] ,'��')">
									<td align="right"><xsl:value-of select="format-number(.,substring-after(/root/t2head/tr[1]/td[$c] ,'��'))"/></td>
								</xsl:if>
								<xsl:if test="not(contains( /root/t2head/tr[1]/td[$c] ,'��'))">
									<td align="left"><xsl:value-of select="."/></td>
								</xsl:if>
							</xsl:if>
							<xsl:if test="string-length($c)=0">
								<td></td>
							</xsl:if>
						</xsl:for-each>
						
						<xsl:for-each select="/root/tbody/tr[$rownum * 2 ]/td"> 
						<xsl:variable name="cc" select="position()"/>
						<xsl:if test="string-length($cc)&gt;0">
							<xsl:if test="contains( /root/t2head/tr[1]/td[$cc] ,'��')">
								<td align="right"><xsl:value-of select="format-number(.,substring-after(/root/t2head/tr[1]/td[$cc] ,'��'))"/></td>
							</xsl:if>
							<xsl:if test="not(contains( /root/t2head/tr[1]/td[$cc] ,'��'))">
								<td align="left"><xsl:value-of select="."/></td>
							</xsl:if>
						</xsl:if>
						<xsl:if test="string-length($cc)=0">
							<td></td>
						</xsl:if>
					</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
