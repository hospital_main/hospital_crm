<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/wage/distribute/bank/queryView5.xsl,v 1.1 2013/09/25 08:48:42 yaoqingsong Exp $
 $Author: yaoqingsong $
 $Date: 2013/09/25 08:48:42 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<xsl:variable  name="pageCnt" select="/root/t2head/tr[1]/td[last()]"/>
			<thead>
				<tr noWrap='true'>
					<td style="colspan:colcount;fontsize:maintitle"></td>
					<xsl:for-each select="/root/t2head/tr[1]/td[position()&gt;2]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
					
					
					<xsl:for-each select="/root/t2head/tr[1]/td[position()&gt;1]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
					
				</tr>
				<tr noWrap='true'>
					<td style="colspan:colcount;fontsize:maintitle"></td>
					<xsl:for-each select="/root/t2head/tr[1]/td[position()&gt;2]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
					
					
					<xsl:for-each select="/root/t2head/tr[1]/td[position()&gt;1]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
					
				</tr>
				<tr noWrap='true'>
					<xsl:for-each select="/root/t2head/tr[1]/td[position() &lt; last()]">
						<td  nowrap='true' style='fontsize:coltitle'>
							<xsl:if test="contains( . ,'‖')">
								<xsl:value-of select="substring-before(. ,'‖')"/>
							</xsl:if>
							<xsl:if test="not(contains( . ,'‖'))">
								<xsl:value-of select="."/>
							</xsl:if>
						</td>
						
					</xsl:for-each>
					
					
					<xsl:for-each select="/root/t2head/tr[1]/td[position() &lt; last()]">
						<td  nowrap='true' style='fontsize:coltitle'>
							<xsl:if test="contains( . ,'‖')">
								<xsl:value-of select="substring-before(. ,'‖')"/>
							</xsl:if>
							<xsl:if test="not(contains( . ,'‖'))">
								<xsl:value-of select="."/>
							</xsl:if>
						</td>
					</xsl:for-each>
					
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr[position() mod 2 = 1 and td[1]!='合计']">
					<xsl:variable name="rownum" select="position()"/>
					<xsl:variable name="vhlastrow" select="last()"/>
					
					<tr>
						<xsl:for-each select="td"> 
							<xsl:variable name="c" select="position()"/>
							<xsl:if test="string-length($c)&gt;0">
								<xsl:if test="contains( /root/t2head/tr[1]/td[$c] ,'‖')">
									<td align="right"><xsl:value-of select="format-number(.,substring-after(/root/t2head/tr[1]/td[$c] ,'‖'))"/></td>
								</xsl:if>
								<xsl:if test="not(contains( /root/t2head/tr[1]/td[$c] ,'‖'))">
									<td align="left"><xsl:value-of select="."/></td>
								</xsl:if>
							</xsl:if>
							<xsl:if test="string-length($c)=0">
								<td></td>
							</xsl:if>
						</xsl:for-each>
						
						<xsl:for-each select="/root/tbody/tr[position()= $rownum * 2 and td[1]!='合计' ]/td"> 
						<xsl:variable name="cc" select="position()"/>
						<xsl:if test="string-length($cc)&gt;0">
							<xsl:if test="contains( /root/t2head/tr[1]/td[$cc] ,'‖')">
								<td align="right"><xsl:value-of select="format-number(.,substring-after(/root/t2head/tr[1]/td[$cc] ,'‖'))"/></td>
							</xsl:if>
							<xsl:if test="not(contains( /root/t2head/tr[1]/td[$cc] ,'‖'))">
								<td align="left"><xsl:value-of select="."/></td>
							</xsl:if>
						</xsl:if>
						<xsl:if test="string-length($cc)=0">
							<td></td>
						</xsl:if>
					</xsl:for-each>
					</tr>
					
					<!--确定合计行-->
					<xsl:if test="$rownum mod $pageCnt = 0 or $vhlastrow = $rownum">
					<tr>
					<xsl:for-each select="/root/tbody/tr[position() = 3]/td">
							<xsl:variable name="d" select="position()"/>
							<xsl:if test="string-length($d)&gt;0">
								<xsl:if test="contains( /root/t2head/tr[1]/td[$d] ,'‖')">
									<td align="right">
									</td>
								</xsl:if>
								<xsl:if test="not(contains( /root/t2head/tr[1]/td[$d] ,'‖'))">
									<xsl:if test="position() = 1 ">
									<td>本页合计2</td>
									</xsl:if>
									<xsl:if test="position() != 1 ">
									<td>
									</td>
									</xsl:if>
								</xsl:if>
							</xsl:if>
							<xsl:if test="string-length($d)=0">
								<td></td>
							</xsl:if>
					</xsl:for-each>
					
					<xsl:for-each select="/root/tbody/tr[position() = 3]/td">
							<xsl:variable name="dd" select="position()"/>
							<xsl:if test="string-length($dd)&gt;0">
								<xsl:if test="contains( /root/t2head/tr[1]/td[$dd] ,'‖')">
									<td align="right">
							
									<xsl:if test="$rownum mod $pageCnt = 0 and $vhlastrow != $rownum">
									<xsl:value-of select="format-number(sum(/root/tbody/tr[ td[1]!='合计' and position()&gt;=1  and position() &gt;= 2*$rownum+1 - $pageCnt *2 and position() &lt; 2*$rownum+1 ]/td[$dd]),substring-after(/root/t2head/tr[1]/td[$dd] ,'‖'))"/>
									</xsl:if>
									<xsl:if test="$vhlastrow = $rownum and $rownum mod $pageCnt != 0" >
									<xsl:value-of select="format-number(sum(/root/tbody/tr[ td[1]!='合计' and position() &gt; floor(2*$rownum div (2*$pageCnt)) *2*$pageCnt ]/td[$dd]),substring-after(/root/t2head/tr[1]/td[$dd] ,'‖'))"/>
									</xsl:if>
									
									</td>
								</xsl:if>
								<xsl:if test="not(contains( /root/t2head/tr[1]/td[$dd] ,'‖'))">
									<xsl:if test="position() = 1 ">
									<td></td>
									</xsl:if>
									<xsl:if test="position() != 1 ">
									<td>
									</td>
									</xsl:if>
								</xsl:if>
							</xsl:if>
							<xsl:if test="string-length($dd)=0">
								<td></td>
							</xsl:if>
					</xsl:for-each>
					
				</tr>
				</xsl:if>
				<!--确定合计行结束-->	
				
			
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
