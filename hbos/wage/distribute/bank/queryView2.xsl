<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/wage/distribute/bank/queryView2.xsl,v 1.3 2013/09/25 08:44:24 yaoqingsong Exp $
 $Author: yaoqingsong $
 $Date: 2013/09/25 08:44:24 $
 $Revision: 1.3 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<xsl:variable  name="pageCnt" select="/root/t2head/tr[1]/td[last()]"/>			
			<thead>
				<tr noWrap='true'>
					<td style="colspan:colcount;fontsize:maintitle"></td>
					<xsl:for-each select="/root/t2head/tr[1]/td[position()&gt;1 and position() &lt; last()]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
				</tr>
				<tr noWrap='true'>
					<td style="colspan:colcount;fontsize:maintitle"></td>
					<xsl:for-each select="/root/t2head/tr[1]/td[position()&gt;1 and position() &lt; last()]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
				</tr>
				<tr noWrap='true'>
					<xsl:for-each select="/root/t2head/tr[1]/td">
						<td  nowrap='true' style='fontsize:coltitle'>
							<xsl:if test="contains( . ,'‖')">
								<xsl:value-of select="substring-before(. ,'‖')"/>
							</xsl:if>
							<xsl:if test="not(contains( . ,'‖'))">
								<xsl:value-of select="."/>
							</xsl:if>
						</td>
					</xsl:for-each>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<xsl:variable name="rownum" select="position()"/>
				<xsl:variable name="vhlastrow" select="last()"/>
					
					<tr>
						<xsl:for-each select="td"> 
							<xsl:variable name="c" select="position()"/>
							<xsl:if test="string-length($c)&gt;0">
								<xsl:if test="contains( /root/t2head/tr[1]/td[$c] ,'‖')">
									<td align="right"><xsl:value-of select="format-number(.,substring-after(/root/t2head/tr[1]/td[$c] ,'‖'))"/></td>
								</xsl:if>
								<xsl:if test="not(contains( /root/t2head/tr[1]/td[$c] ,'‖'))">
									<td align="left"><xsl:value-of select="."/></td>
								</xsl:if>
							</xsl:if>
							<xsl:if test="string-length($c)=0">
								<td></td>
							</xsl:if>
						</xsl:for-each>
					</tr>
					
					<!--本页合计行-->

<!--<xsl:if test="/root/tbody/tr[position() = $vhlastrow ]/td[1]!='合计'">-->
		<xsl:if test=" /root/tbody/tr[position() = $vhlastrow ]/td[1]='合计' and (($rownum mod $pageCnt = 0 and $vhlastrow != $rownum) or ($vhlastrow = $rownum+1 and $rownum mod $pageCnt != 0)) ">
				
				<tr>							
					<xsl:for-each select="/root/tbody/tr[position() = 3]/td">
							<xsl:variable name="d" select="position()"/>
							<xsl:if test="string-length($d)&gt;0">
								<xsl:if test="contains( /root/t2head/tr[1]/td[$d] ,'‖')">
									<td align="right">

										<xsl:if test="$rownum mod $pageCnt = 0 and $vhlastrow != $rownum ">
											<xsl:value-of select="format-number(sum(/root/tbody/tr[position() &gt;= $rownum+1 - $pageCnt and position() &lt;$rownum+1 ]/td[$d]),substring-after(/root/t2head/tr[1]/td[$d] ,'‖'))"/>
										</xsl:if>
										<xsl:if test="$vhlastrow = $rownum+1 and $rownum mod $pageCnt != 0">
											<xsl:value-of select="format-number(sum(/root/tbody/tr[position() &gt;= floor($rownum div ($pageCnt)) *$pageCnt+1  and position()!=$vhlastrow]/td[$d]),substring-after(/root/t2head/tr[1]/td[$d] ,'‖'))"/>
										</xsl:if>

									</td>
								</xsl:if>
								<xsl:if test="not(contains( /root/t2head/tr[1]/td[$d] ,'‖'))">
									<xsl:if test="position() = 1 ">
									<td>本页合计</td>
									</xsl:if>
									<xsl:if test="position() != 1 ">
									<td>
									</td>
									</xsl:if>
								</xsl:if>
							</xsl:if>
							<xsl:if test="string-length($d)=0">
								<td></td>
							</xsl:if>		
						</xsl:for-each>			
				</tr>	
			</xsl:if>
			
			
			<xsl:if test=" /root/tbody/tr[position() = $vhlastrow ]/td[1]!='合计' and ($rownum mod $pageCnt = 0 or $vhlastrow = $rownum) ">
				
				<tr>							
					<xsl:for-each select="/root/tbody/tr[position() = 3]/td">
							<xsl:variable name="d" select="position()"/>
							<xsl:if test="string-length($d)&gt;0">
								<xsl:if test="contains( /root/t2head/tr[1]/td[$d] ,'‖')">
									<td align="right">

										<xsl:if test="$rownum mod $pageCnt = 0 ">
											<xsl:value-of select="format-number(sum(/root/tbody/tr[position() &gt;= $rownum+1 - $pageCnt and position() &lt;$rownum+1 ]/td[$d]),substring-after(/root/t2head/tr[1]/td[$d] ,'‖'))"/>
										</xsl:if>
										<xsl:if test="$vhlastrow = $rownum and $rownum mod $pageCnt != 0">
											<xsl:value-of select="format-number(sum(/root/tbody/tr[position() &gt;= floor($rownum div ($pageCnt)) *$pageCnt+1 ]/td[$d]),substring-after(/root/t2head/tr[1]/td[$d] ,'‖'))"/>
										</xsl:if>

									</td>
								</xsl:if>
								<xsl:if test="not(contains( /root/t2head/tr[1]/td[$d] ,'‖'))">
									<xsl:if test="position() = 1 ">
									<td>本页合计</td>
									</xsl:if>
									<xsl:if test="position() != 1 ">
									<td>
									</td>
									</xsl:if>
								</xsl:if>
							</xsl:if>
							<xsl:if test="string-length($d)=0">
								<td></td>
							</xsl:if>		
						</xsl:for-each>			
				</tr>	
			</xsl:if>





					
				</xsl:for-each>
				
				
				
				<!--<tr>
					<xsl:for-each select="/root/tbody/tr[position() = 3]/td">
							<xsl:variable name="d" select="position()"/>
							<xsl:if test="string-length($d)&gt;0">
								<xsl:if test="contains( /root/t2head/tr[1]/td[$d] ,'‖')">
									<td align="right">
									<xsl:value-of select="format-number(sum(/root/tbody/tr/td[$d]),substring-after(/root/t2head/tr[1]/td[$d] ,'‖'))"/>
									</td>
								</xsl:if>
								<xsl:if test="not(contains(/root/t2head/tr[1]/td[$d] ,'‖'))">
									<xsl:if test="position() = 1 ">
									<td>合计2 </td>
									</xsl:if>
									<xsl:if test="position() != 1 ">
									<td>
									</td>
									</xsl:if>
								</xsl:if>
							</xsl:if>
							<xsl:if test="string-length($d)=0">
								<td></td>
							</xsl:if>
					</xsl:for-each>
								
				</tr>-->
							</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
