<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/wage/distribute/table/queryView.xsl,v 1.1 2012/03/12 01:56:36 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:56:36 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<thead>
				<tr noWrap='true'>
					<td style="colspan:colcount;fontsize:maintitle"></td>
					<xsl:for-each select="/root/t2head/tr[1]/td[position()&gt;1]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
				</tr>
				<tr noWrap='true'>
					<td style="colspan:4;fontsize:subtitle;align:left"></td>
					<xsl:for-each select="/root/t2head/tr[1]/td[position()&gt;1 and position()&lt;5]">
						<td nowrap='true' style="display:none">'</td>
					</xsl:for-each>
					<xsl:if test="count(/root/t2head/tr[1]/td) &gt; 4">
						<td>
							<xsl:attribute name="style">colspan:<xsl:value-of select="count(/root/t2head/tr[1]/td) - 4 "/>;fontsize:subtitle;align:right</xsl:attribute>
						</td>
						<xsl:for-each select="/root/t2head/tr[1]/td[position()&gt;4 ]">
							<td nowrap='true' style="display:none">-</td>
						</xsl:for-each>
					</xsl:if>
				</tr>
				<tr noWrap='true'>
					<xsl:for-each select="/root/t2head/tr[1]/td">
						<td  nowrap='true' style='fontsize:coltitle'>
							<xsl:if test="contains( . ,'��')">
								<xsl:value-of select="substring-before(. ,'��')"/>
							</xsl:if>
							<xsl:if test="not(contains( . ,'��'))">
								<xsl:value-of select="."/>
							</xsl:if>
						</td>
					</xsl:for-each>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td"> 
							<xsl:variable name="c" select="position()"/>
							<xsl:if test="string-length($c)&gt;0">
								<xsl:if test="contains( /root/t2head/tr[1]/td[$c] ,'��')">
									<td align="right"><xsl:value-of select="format-number(.,substring-after(/root/t2head/tr[1]/td[$c] ,'��'))"/></td>
								</xsl:if>
								<xsl:if test="not(contains( /root/t2head/tr[1]/td[$c] ,'��'))">
									<td align="left"><xsl:value-of select="."/></td>
								</xsl:if>
							</xsl:if>
							<xsl:if test="string-length($c)=0">
								<td></td>
							</xsl:if>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
