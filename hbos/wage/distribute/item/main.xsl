<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<xsl:for-each select="/root/t2head/tr[1]/td[position() &lt; last()]">
					<th nowrap='true'>
						<xsl:if test="contains( . ,'��')">
							<xsl:value-of select="substring-before(. ,'��')"/>
						</xsl:if>
						<xsl:if test="not(contains( . ,'��'))">
							<xsl:value-of select="."/>
						</xsl:if>
					</th>
				</xsl:for-each>
			</tr>
		</thead>                 
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position() &lt; last()]"> 
						<xsl:variable name="c" select="position()"/>
						<xsl:if test="string-length($c)&gt;0">
							<xsl:if test="contains( /root/t2head/tr[1]/td[$c] ,'��')">
								<td align="right">
									<xsl:if test=".!=''">
										<xsl:value-of select="format-number(.,substring-after(/root/t2head/tr[1]/td[$c] ,'��'))"/>
									</xsl:if>
									<xsl:if test=".=''">
										0.00
									</xsl:if>
								</td>
							</xsl:if>
							<xsl:if test="not(contains( /root/t2head/tr[1]/td[$c] ,'��'))">
								<td align="left"><xsl:value-of select="."/></td>
							</xsl:if>
						</xsl:if>
						<xsl:if test="string-length($c)=0">
							<td>j</td>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>