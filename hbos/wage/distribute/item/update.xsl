<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>��Ŀ</th>
				<th nowrap='true'>˳��</th>
				<th nowrap='true'>����</th>
			</tr>
		</thead>                          
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:variable name="pp" select="position()"/>
					<xsl:for-each select="td[position() &gt; 1]"> 
						<xsl:choose>
							<xsl:when test="position()=2">
								<td >
									<input type="text" style="display:none">
										<xsl:attribute name="value"><xsl:value-of select="../td[1]"/></xsl:attribute>
									</input>
									<input type="text" name="dd" class="inputDecimal" point="0" maxInput="12" >
									<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
									</input>
								</td>
							</xsl:when>
							<xsl:when test="position()=3">
								<td >
									<input type="checkbox"  >
										<xsl:if test=" . = 1 "><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
									</input>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>