<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/wage/distribute/item/queryView_3.xsl,v 1.1 2012/08/08 06:23:04 linaikun Exp $
 $Author: linaikun $
 $Date: 2012/08/08 06:23:04 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<thead>
				<xsl:variable name="colNum" select="/root/t2head/tr[1]/td[last()]"/>
				<xsl:variable name="totCol" select="count(/root/t2head/tr[1]/td) -1 "/> 
				<xsl:variable name="realTotCol" select="count(/root/t2head/tr[1]/td)"/>
				<tr noWrap='true'>
					<td style="fontsize:maintitle;colspan:colcount"></td>
					<xsl:for-each select="/root/t2head/tr[1]/td[position() &gt; 1 and position() &lt; $colNum + 1 and position() &lt; $totCol  ]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
				</tr>
			</thead>
			
			<tbody>
				<xsl:for-each select="/root/tbody/tr[position() != last()]">
				
					<xsl:variable name="rowNum" select="position()"/>
					
					<xsl:if test="$rowNum =1 or /root/tbody/tr[$rowNum - 1]/td[1] != ./td[1]">
						<tr noWrap='true' align='left'>
							<td align='left' style="colspan:colcount;align:left">
								<xsl:if test="$rowNum !=1">
									<xsl:attribute name="style">pagebreak:true;colspan:colcount</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="./td[1]"/>
							</td>
						
						</tr>
					</xsl:if>
					
					<xsl:for-each select="td[1 &lt; position() ]"> 
						<xsl:variable name="c" select="position()"/>
						
						<xsl:if test="$c mod $colNum = 0 or $c=$totCol or $colNum &gt; $totCol">
							<tr noWrap='true'>
								<xsl:if test="$c mod $colNum = 0 and $c != $totCol">
									<xsl:for-each select="/root/t2head/tr[1]/td[position() &gt; $c - $colNum + 1 and position() &lt; $c + 2]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'��')">
												<xsl:value-of select="substring-before(. ,'��')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum = 0 and $c = $totCol">
									<xsl:for-each select="/root/t2head/tr[1]/td[position() &gt; $c - $colNum + 1 and position() &lt; $c + 2 and position() != $realTotCol]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'��')">
												<xsl:value-of select="substring-before(. ,'��')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum != 0 and $c = $totCol">
									<xsl:for-each select="/root/t2head/tr[1]/td[position() &gt; $c - ($c mod $colNum) + 1 and position() &lt; $c + 2 and position() != $realTotCol]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'��')">
												<xsl:value-of select="substring-before(. ,'��')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
							</tr>
							
							<tr noWrap='true'>
								<xsl:if test="$c mod $colNum = 0 and $c != $totCol">
									<xsl:for-each select="/root/tbody/tr[$rowNum]/td[position() &gt; $c - $colNum + 1 and position() &lt; $c + 2]">
										<xsl:variable name="tc" select="$c - $colNum + position() + 1 "/>
										<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
											<xsl:if test="contains( /root/t2head/tr[1]/td[$tc] ,'��')">
												<xsl:attribute name="align">right</xsl:attribute>
												<xsl:if test=". != 0 and .!=0.00 and .!= 0.0">
													<xsl:value-of select="format-number(.,substring-after(/root/t2head/tr[1]/td[$tc] ,'��'))"/>
												</xsl:if>
											</xsl:if>
											<xsl:if test="not(contains( /root/t2head/tr[1]/td[$tc] ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum = 0 and $c = $totCol">
									<xsl:for-each select="/root/tbody/tr[$rowNum ]/td[position() &gt; $c - $colNum + 1 and position() &lt; $c + 2 and position() != $realTotCol]">
										<xsl:variable name="tc" select="$c - $colNum + position() + 1"/>
										<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
											<xsl:if test="contains( /root/t2head/tr[1]/td[$tc] ,'��')">
												<xsl:attribute name="align">right</xsl:attribute>
												<xsl:if test=". != 0 and .!=0.00 and .!= 0.0">
													<xsl:value-of select="format-number(.,substring-after(/root/t2head/tr[1]/td[$tc] ,'��'))"/>
												</xsl:if>
											</xsl:if>
											<xsl:if test="not(contains( /root/t2head/tr[1]/td[$tc] ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum != 0 and $c = $totCol">
									<xsl:for-each select="/root/tbody/tr[$rowNum]/td[position() &gt; $c - ($c mod $colNum) + 1 and position() &lt; $c + 2 and position() != $realTotCol]">
										<xsl:variable name="tc" select="$c - ($c mod $colNum) + position() + 1 "/>
										<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
											<xsl:if test="contains( /root/t2head/tr[1]/td[$tc] ,'��')">
												<xsl:attribute name="align">right</xsl:attribute>
												<xsl:if test=". != 0 and .!=0.00 and .!= 0.0">
													<xsl:value-of select="format-number(.,substring-after(/root/t2head/tr[1]/td[$tc] ,'��'))"/>
												</xsl:if>
											</xsl:if>
											<xsl:if test="not(contains( /root/t2head/tr[1]/td[$tc] ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
							</tr>
						</xsl:if>
					</xsl:for-each>
					<xsl:if test="/root/annex/annex_emptyrow = 1 ">
						<tr>
						 <td height="3" style="pagebreak:deny"><xsl:attribute name="colspan"><xsl:value-of select='count(/root/t2head/tr[1]/td)'/></xsl:attribute></td>
						</tr>
					</xsl:if>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
