<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>

		</thead>                 
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position() != 1]">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=3 or position()=4">
								<td ><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>