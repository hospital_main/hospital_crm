<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/wage/emp/info/queryView.xsl,v 1.1 2012/03/12 01:56:37 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:56:37 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
		<thead>
			<tr>
				<td style="colspan:15;fontsize:maintitle">职工信息查询</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr>
				<td style='fontsize:coltitle'>职工编码</td>
				<td nowrap='true' style='fontsize:coltitle'>职工姓名</td>
				<td nowrap='true' style='fontsize:coltitle'>性别</td>
				<td nowrap='true' style='fontsize:coltitle'>出生年月</td>
				<td nowrap='true' style='fontsize:coltitle'>国籍</td>
				<td nowrap='true' style='fontsize:coltitle'>学历</td>
				<td nowrap='true' style='fontsize:coltitle'>身份证号</td>
				<td nowrap='true' style='fontsize:coltitle'>入职时间</td>
				<td nowrap='true' style='fontsize:coltitle'>职称</td>
				<td nowrap='true' style='fontsize:coltitle'>职位</td>
				<td nowrap='true' style='fontsize:coltitle'>类别</td>
				<td nowrap='true' style='fontsize:coltitle'>工作电话</td>
				<td nowrap='true' style='fontsize:coltitle'>手机</td>
				<td nowrap='true' style='fontsize:coltitle'>email</td>
				<td nowrap='true' style='fontsize:coltitle'>是否停用</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=3 or position()=4">
								<td ><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
