<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style=''>职工编码</th>
				<th nowrap='true'>职工姓名</th>
				<th nowrap='true'>性别</th>
				<th nowrap='true'>出生年月</th>
				<th nowrap='true'>国籍</th>
				<th nowrap='true'>学历</th>
				<th nowrap='true'>身份证号</th>
				<th nowrap='true'>入职时间</th>
				<th nowrap='true'>职称</th>
				<th nowrap='true'>职位</th>
				<th nowrap='true'>类别</th>
				<th nowrap='true'>工作电话</th>
				<th nowrap='true'>手机</th>
				<th nowrap='true'>email</th>
				<xsl:for-each select="/root/tbody/tr[position() = 1]/td[.!='']">
				<th nowrap="true"><xsl:value-of select="."></xsl:value-of></th>
				</xsl:for-each>
				<th nowrap='true'>是否停用</th>
			</tr>
		</thead>                 
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position() != 1]">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=3 or position()=4">
								<td ><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>