<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th style='display:none;width:40px;'><input type='checkbox'/></th>
  			<th nowrap='true'>职工编码</th>
  			<th nowrap='true'>姓名</th>
  			<th nowrap='true'>身份证号</th>
  			<th nowrap='true'>代发类别</th>
  			<th nowrap='true'>工资账号</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
         <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                  <a href="#">
									  <xsl:attribute name="onclick">
									    javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:350px;dialogHeight:210px')
									  </xsl:attribute>
                 		<xsl:value-of select="."/>
									</a>
                </td>
              </xsl:when>
              <xsl:when test="position()=4">
                <td align="center">
                  <xsl:value-of select="."/>
                </td>
              </xsl:when>
							<xsl:when test="position()=5 ">
                <td align="right"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>