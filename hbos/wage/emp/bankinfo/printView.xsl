<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
		  <colgroup>		       
				<col style = 'width:70'/>
				<col style = 'width:80'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:70'/>
			</colgroup>  		
    	<thead>
    		<tr noWrap='true' class='mainHead'>
    		<td style='colspan:12;fontsize:maintitle'>支票登记</td>
    			<td style="display:none"></td>
    		  <td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
  	  	</tr>
  	  	<tr noWrap='true' class='mainHead'>
    			<td nowrap='true'>银行名称</td>
    			<td nowrap='true'>银行账户</td>
    			<td nowrap='true'>支票类型</td>
    			<td nowrap='true'>登记日期</td>
    			<td nowrap='true'>起始号码</td>
    			<td nowrap='true'>结束号码</td>
    			<td nowrap='true'>总张数</td>
    			<td nowrap='true'>未用张数</td>
    			<td nowrap='true'>已领用张数</td>
    			<td nowrap='true'>已报销张数</td>
    			<td nowrap='true'>已作废张数</td>
    			<td nowrap='true'>登记人</td>
  	  	</tr>
    	</thead>
    	<tbody>
  	    <xsl:for-each select="/root/tbody/tr">
          <tr>
            <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=4">
                  <td class="centerText">
                    <xsl:value-of select="."/>
                  </td>
                </xsl:when>
  							<xsl:when test="position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11">
                  <td class="numberText"><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:when test="position()>12">
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>            
    			  </xsl:for-each>
    			</tr>
     		</xsl:for-each>
   		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>