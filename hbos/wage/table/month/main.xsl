<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>工资项编码</th> 
				<th nowrap='true'>工资项名称</th>
				<th nowrap='true'>基期</th> 
				<th nowrap='true'>比较期</th>
				<th nowrap='true'>增减额</th>
				<th nowrap='true'>增减比例(%)</th>
			</tr>     
		</thead>        
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">   
						
							<xsl:choose>
								<xsl:when test="position() = 3 or position() = 4 or position() = 5 or position() = 6">
								<td align="right">
									<xsl:value-of select="format-number(.,'##,##0.00')"/>
								</td>	
								</xsl:when>
								<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>  
								</td>	
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>

