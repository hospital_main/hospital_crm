<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN =' '/>
	<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap='true' class='mainHead'>
					<td style="fontsize:maintitle;colspan:6"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
			</tr>

			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>工资项编码</td>
				<td nowrap='true'>工资项名称</td>
				<td nowrap='true'>基期</td>
				<td nowrap='true'>比较期</td>
				<td nowrap='true'>增减额</td>
				<td nowrap='true'>增减比例(%)</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">

							<xsl:choose>
								<xsl:when test="position() = 3 or position() = 4 or position() = 5 or position() = 6">
								<td class="numberText" style="align:right">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:when>
								<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
								</xsl:otherwise>
							</xsl:choose>

					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>

