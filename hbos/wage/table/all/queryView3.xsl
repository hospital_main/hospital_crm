<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/wage/table/all/queryView3.xsl,v 1.2 2015/02/03 05:39:33 zhubingrui Exp $
 $Author: zhubingrui $
 $Date: 2015/02/03 05:39:33 $
 $Revision: 1.2 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<thead>
				<xsl:variable name="colNum" select="/root/tbody/tr[1]/td[last()]"/>
				<xsl:variable name="totCol" select="count(/root/tbody/tr[1]/td) -1 "/>
				<xsl:variable name="total_num" select="/root/record/total"/> 
				<tr noWrap='true'>
					<td style="fontsize:maintitle;colspan:colcount"></td>
					<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1 and position() &lt; $colNum + 1 and position() &lt; $totCol + 1 ]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
				</tr>
			</thead>
			
			<tbody>
	
					
				<xsl:for-each select="/root/tbody/tr[position() &lt; $total_num]">
					<xsl:variable name="contrast_item" select="/root/annex/contrast_item"/>	
					<xsl:variable name="rowNum" select="position()+1"/>
					<xsl:variable name="rowNum_1" select="position()*2"/>
					<xsl:variable name="rowNum_2" select="position()*2+1"/>
					
					<xsl:for-each select="td[position() &lt; last()]"> 
						
						<xsl:variable name="c" select="position()"/>
						
						<xsl:if test="$c mod $colNum = 0 or $c=$totCol">				
								
						
							
							<xsl:if test="$contrast_item = 1 and $rowNum_1 &lt; $total_num">
								<xsl:if test="$c mod $colNum = 0 and $c != $totCol ">
									<tr noWrap='true'>
									<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'��')">
												<xsl:value-of select="substring-before(. ,'��')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
									</tr>
								</xsl:if>
								<xsl:if test="$c mod $colNum = 0 and $c = $totCol">
									<tr noWrap='true'>
									<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'��')">
												<xsl:value-of select="substring-before(. ,'��')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
									</tr>
								</xsl:if>
								<xsl:if test="$c mod $colNum != 0 and $c = $totCol">
									<tr noWrap='true'>
									<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; $c - ($c mod $colNum)  and position() &lt; $c + 1 ]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'��')">
												<xsl:value-of select="substring-before(. ,'��')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
									</tr>
								</xsl:if>
							<tr noWrap='true'>
								<xsl:if test="$c mod $colNum = 0 and $c != $totCol">
									<xsl:for-each select="/root/tbody/tr[$rowNum_1]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<xsl:variable name="tc" select="$c - $colNum + position() "/>
										<xsl:choose>
											<xsl:when test="../td[1]=../../tr[$rowNum_2]/td[1] and position()= 1 and $c = $colNum and $c > 2">
												<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
													<xsl:attribute name="rowspan">2</xsl:attribute>
													<xsl:if test="contains( /root/tbody/tr[1]/td[$tc] ,'��')">
														<xsl:if test=".!=''">
															<xsl:attribute name="align">right</xsl:attribute>
															<xsl:value-of select="format-number(.,substring-after(/root/tbody/tr[1]/td[$tc] ,'��'))"/>
														</xsl:if>
														<xsl:if test=".=''">
															<xsl:attribute name="align">right</xsl:attribute>0.00
														</xsl:if>
													</xsl:if>
													<xsl:if test="not(contains( /root/tbody/tr[1]/td[$tc] ,'��'))">
														<xsl:value-of select="."/>
													</xsl:if>
												</td>
											</xsl:when>
											<xsl:when test="../td[1]=../../tr[$rowNum_2]/td[1] and position()= 2 and $c = $colNum and $c > 2">
												<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
													<xsl:attribute name="rowspan">2</xsl:attribute>
													<xsl:if test="contains( /root/tbody/tr[1]/td[$tc] ,'��')">
														<xsl:if test=".!=''">
															<xsl:attribute name="align">right</xsl:attribute>
															<xsl:value-of select="format-number(.,substring-after(/root/tbody/tr[1]/td[$tc] ,'��'))"/>
														</xsl:if>
														<xsl:if test=".=''">
															<xsl:attribute name="align">right</xsl:attribute>0.00
														</xsl:if>
													</xsl:if>
													<xsl:if test="not(contains( /root/tbody/tr[1]/td[$tc] ,'��'))">
														<xsl:value-of select="."/>
													</xsl:if>
												</td>
											</xsl:when>
											<xsl:otherwise>
												<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
													<xsl:if test="contains( /root/tbody/tr[1]/td[$tc] ,'��')">
														<xsl:if test=".!=''">
															<xsl:attribute name="align">right</xsl:attribute>
															<xsl:value-of select="format-number(.,substring-after(/root/tbody/tr[1]/td[$tc] ,'��'))"/>
														</xsl:if>
														<xsl:if test=".=''">
															<xsl:attribute name="align">right</xsl:attribute>0.00
														</xsl:if>
													</xsl:if>
													<xsl:if test="not(contains( /root/tbody/tr[1]/td[$tc] ,'��'))">
														<xsl:value-of select="."/>
													</xsl:if>
												</td>
											</xsl:otherwise>
											</xsl:choose>
										
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum = 0 and $c = $totCol">
									<xsl:for-each select="/root/tbody/tr[$rowNum_1 ]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<xsl:variable name="tc" select="$c - $colNum + position() "/>
										<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
											<xsl:if test="contains( /root/tbody/tr[1]/td[$tc] ,'��')">
												<xsl:if test=".!=''">
													<xsl:attribute name="align">right</xsl:attribute>
													<xsl:value-of select="format-number(.,substring-after(/root/tbody/tr[1]/td[$tc] ,'��'))"/>
												</xsl:if>
												<xsl:if test=".=''">
													<xsl:attribute name="align">right</xsl:attribute>0.00
												</xsl:if>
											</xsl:if>
											<xsl:if test="not(contains( /root/tbody/tr[1]/td[$tc] ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum != 0 and $c = $totCol">
									<xsl:for-each select="/root/tbody/tr[$rowNum_1]/td[position() &gt; $c - ($c mod $colNum)  and position() &lt; $c + 1 ]">
										<xsl:variable name="tc" select="$c - ($c mod $colNum) + position() "/>
										<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
											<xsl:if test="contains( /root/tbody/tr[1]/td[$tc] ,'��')">
												<xsl:if test=".!=''">
													<xsl:attribute name="align">right</xsl:attribute>
													<xsl:value-of select="format-number(.,substring-after(/root/tbody/tr[1]/td[$tc] ,'��'))"/>
												</xsl:if>
												<xsl:if test=".=''">
													<xsl:attribute name="align">right</xsl:attribute>0.00
												</xsl:if>
											</xsl:if>
											<xsl:if test="not(contains( /root/tbody/tr[1]/td[$tc] ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
							</tr>
							
							<tr noWrap='true'>
							<xsl:if test="$c mod $colNum = 0 and $c != $totCol">
								<xsl:for-each select="/root/tbody/tr[$rowNum_2]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
									<xsl:variable name="tc" select="$c - $colNum + position() "/>
									<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
										<xsl:if test="contains( /root/tbody/tr[1]/td[$tc] ,'��')">
											<xsl:if test=".!=''">
												<xsl:attribute name="align">right</xsl:attribute>
												<xsl:value-of select="format-number(.,substring-after(/root/tbody/tr[1]/td[$tc] ,'��'))"/>
											</xsl:if>
											<xsl:if test=".=''">
												<xsl:attribute name="align">right</xsl:attribute>0.00
											</xsl:if>
										</xsl:if>
										<xsl:if test="not(contains( /root/tbody/tr[1]/td[$tc] ,'��'))">
											<xsl:value-of select="."/>
										</xsl:if>
									</td>
								</xsl:for-each>
							</xsl:if>
							<xsl:if test="$c mod $colNum = 0 and $c = $totCol">
								<xsl:for-each select="/root/tbody/tr[$rowNum_2 ]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
									<xsl:variable name="tc" select="$c - $colNum + position() "/>
									<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
										<xsl:if test="contains( /root/tbody/tr[1]/td[$tc] ,'��')">
											<xsl:if test=".!=''">
												<xsl:attribute name="align">right</xsl:attribute>
												<xsl:value-of select="format-number(.,substring-after(/root/tbody/tr[1]/td[$tc] ,'��'))"/>
											</xsl:if>
											<xsl:if test=".=''">
												<xsl:attribute name="align">right</xsl:attribute>0.00
											</xsl:if>
										</xsl:if>
										<xsl:if test="not(contains( /root/tbody/tr[1]/td[$tc] ,'��'))">
											<xsl:value-of select="."/>
										</xsl:if>
									</td>
								</xsl:for-each>
							</xsl:if>
							<xsl:if test="$c mod $colNum != 0 and $c = $totCol">
								<xsl:for-each select="/root/tbody/tr[$rowNum_2]/td[position() &gt; $c - ($c mod $colNum)  and position() &lt; $c + 1 ]">
									<xsl:variable name="tc" select="$c - ($c mod $colNum) + position() "/>
									<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
										<xsl:if test="contains( /root/tbody/tr[1]/td[$tc] ,'��')">
											<xsl:if test=".!=''">
												<xsl:attribute name="align">right</xsl:attribute>
												<xsl:value-of select="format-number(.,substring-after(/root/tbody/tr[1]/td[$tc] ,'��'))"/>
											</xsl:if>
											<xsl:if test=".=''">
												<xsl:attribute name="align">right</xsl:attribute>0.00
											</xsl:if>
										</xsl:if>
										<xsl:if test="not(contains( /root/tbody/tr[1]/td[$tc] ,'��'))">
											<xsl:value-of select="."/>
										</xsl:if>
									</td>
								</xsl:for-each>
							</xsl:if>
						</tr>
					</xsl:if>
					<xsl:if test="$contrast_item = 0">
						<xsl:if test="$c mod $colNum = 0 and $c != $totCol ">
									<tr noWrap='true'>
									<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'��')">
												<xsl:value-of select="substring-before(. ,'��')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
									</tr>
								</xsl:if>
								<xsl:if test="$c mod $colNum = 0 and $c = $totCol">
									<tr noWrap='true'>
									<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'��')">
												<xsl:value-of select="substring-before(. ,'��')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
									</tr>
								</xsl:if>
								<xsl:if test="$c mod $colNum != 0 and $c = $totCol">
									<tr noWrap='true'>
									<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; $c - ($c mod $colNum)  and position() &lt; $c + 1 ]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'��')">
												<xsl:value-of select="substring-before(. ,'��')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
									</tr>
								</xsl:if>
					<tr noWrap='true'>
						<xsl:if test="$c mod $colNum = 0 and $c != $totCol">
							<xsl:for-each select="/root/tbody/tr[$rowNum]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
								<xsl:variable name="tc" select="$c - $colNum + position() "/>
								<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
									<xsl:if test="contains( /root/tbody/tr[1]/td[$tc] ,'��')">
										<xsl:if test=".!=''">
											<xsl:attribute name="align">right</xsl:attribute>
											<xsl:value-of select="format-number(.,substring-after(/root/tbody/tr[1]/td[$tc] ,'��'))"/>
										</xsl:if>
										<xsl:if test=".=''">
											<xsl:attribute name="align">right</xsl:attribute>0.00
										</xsl:if>
									</xsl:if>
									<xsl:if test="not(contains( /root/tbody/tr[1]/td[$tc] ,'��'))">
										<xsl:value-of select="."/>
									</xsl:if>
								</td>
							</xsl:for-each>
						</xsl:if>
						<xsl:if test="$c mod $colNum = 0 and $c = $totCol">
							<xsl:for-each select="/root/tbody/tr[$rowNum ]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
								<xsl:variable name="tc" select="$c - $colNum + position() "/>
								<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
									<xsl:if test="contains( /root/tbody/tr[1]/td[$tc] ,'��')">
										<xsl:if test=".!=''">
											<xsl:attribute name="align">right</xsl:attribute>
											<xsl:value-of select="format-number(.,substring-after(/root/tbody/tr[1]/td[$tc] ,'��'))"/>
										</xsl:if>
										<xsl:if test=".=''">
											<xsl:attribute name="align">right</xsl:attribute>0.00
										</xsl:if>
									</xsl:if>
									<xsl:if test="not(contains( /root/tbody/tr[1]/td[$tc] ,'��'))">
										<xsl:value-of select="."/>
									</xsl:if>
								</td>
							</xsl:for-each>
						</xsl:if>
						<xsl:if test="$c mod $colNum != 0 and $c = $totCol">
							<xsl:for-each select="/root/tbody/tr[$rowNum]/td[position() &gt; $c - ($c mod $colNum)  and position() &lt; $c + 1 ]">
								<xsl:variable name="tc" select="$c - ($c mod $colNum) + position() "/>
								<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
									<xsl:if test="contains( /root/tbody/tr[1]/td[$tc] ,'��')">
										<xsl:if test=".!=''">
											<xsl:attribute name="align">right</xsl:attribute>
											<xsl:value-of select="format-number(.,substring-after(/root/tbody/tr[1]/td[$tc] ,'��'))"/>
										</xsl:if>
										<xsl:if test=".=''">
											<xsl:attribute name="align">right</xsl:attribute>0.00
										</xsl:if>
									</xsl:if>
									<xsl:if test="not(contains( /root/tbody/tr[1]/td[$tc] ,'��'))">
										<xsl:value-of select="."/>
									</xsl:if>
								</td>
							</xsl:for-each>
						</xsl:if>
					</tr>
						
					</xsl:if>
							<!---->
						</xsl:if>
					</xsl:for-each>
					<xsl:if test="/root/annex/annex_emptyrow = 1 ">
						
						<xsl:if test="$contrast_item = '1' and $rowNum_1 &lt; $total_num">
							<tr>
								<td height="3" style="pagebreak:deny"><xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute></td>
							</tr>
						</xsl:if>
						<xsl:if test="$contrast_item = '0'">
							<tr>
								<td height="3" style="pagebreak:deny"><xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute></td>
							</tr>
						</xsl:if>
						
					</xsl:if>
				</xsl:for-each>
			</tbody>
			<tfoot>
				<tr>
					<td style="align:left;fontsize:coltitle;colspan:colcount">
						<xsl:value-of select="/root/annex/bottomTitle_label"/>
					</td>
				</tr> 
			</tfoot>
		</table>
	</xsl:template>
</xsl:stylesheet>
