<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:template match="/">
		<thead>
			<xsl:variable name="bodyrows" select="/root/tbody/tr"/>
			<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=1  ">
	    			<tr noWrap='true' class='mainHead'>
          		<th valign="middle" width='60'  noWrap="true"><xsl:value-of select="/root/tbody/tr[1]/td[1]"/></th>
          		<th valign="middle" width='60'  noWrap="true"><xsl:value-of select="/root/tbody/tr[1]/td[2]"/></th>
		          <xsl:for-each select="td">
		          	<xsl:variable name="cols" select="position()"/>
		          	<xsl:variable name="cols_name" select="."/>
		          	<xsl:choose>
			          	<xsl:when test=" position()>2 and position()!=last()">
		          			<th noWrap="true">
				            	<xsl:value-of select="."/>
		          			</th>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		          
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
		</thead>                 
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="row_index" select="position()"/>
	    	<xsl:choose>
	    		<xsl:when test=" position()>1  and position()!=last() ">
	    			<tr>
		          <xsl:for-each select="td">
		          	<xsl:choose>
		          		<xsl:when test="position()=1 ">
			            	<td width="60"  noWrap="true"><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:when test="position()=2 ">
			            	<td width="80"  noWrap="true"><xsl:value-of select="."/></td>
			            </xsl:when>
						<xsl:when test="position()=last()-1 ">
							<td align="right"  noWrap="true"><xsl:value-of select="."/></td>
						</xsl:when>
			          	<xsl:when test="position() > 2 and position()!=last() and position()!=last()-1 ">
			            	<td  align="right" noWrap="true" >
								<xsl:attribute name="class">numberText</xsl:attribute>
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		          
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
   		<tr>
   			<td colspan="2" style="background:url(../../../../base/themes/default/skins/skins01/images/th_bg.gif) repeat-x; border:1px; border-top:1px solid #999">�ϼƣ�</td>
   			<td style="display:none" ></td>
   			<xsl:for-each select="/root/tbody/tr">
   				<xsl:variable name="last_row" select="last()"/>
        	<xsl:choose>
        		<xsl:when test="position() =$last_row">
	        		<xsl:for-each select="td">
	        			<xsl:choose>
							<xsl:when test="position()=last()-1 ">
								<td align="right" ><xsl:value-of select="."/></td>
							</xsl:when>
			          	<xsl:when test="position()> 2 and position()!=last() ">
			            	<td align="right" >
								<xsl:attribute name="class">numberText</xsl:attribute>
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
			            </xsl:when>

		            </xsl:choose>
	            </xsl:for-each>
	            
	          </xsl:when>
        	</xsl:choose>
	   		</xsl:for-each>
   		</tr>
		</tbody>
	</xsl:template>
</xsl:stylesheet>