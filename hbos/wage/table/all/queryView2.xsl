<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/wage/table/all/queryView2.xsl,v 1.1 2012-04-11 08:01:48 yangqing Exp $
 $Author: yangqing $
 $Date: 2012-04-11 08:01:48 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<thead>
				<xsl:variable name="colNum" select="/root/tbody/tr[1]/td[last()]"/>
				<xsl:variable name="totCol" select="count(/root/tbody/tr[1]/td) -1 "/>
				<xsl:variable name="total_num" select="/root/record/total"/> 
				<tr noWrap='true'>
					<td style="fontsize:maintitle;colspan:colcount"></td>
					<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1 and position() &lt; $colNum + 1 and position() &lt; $totCol + 1 ]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
				</tr>
			</thead>
			
			<tbody>
				<xsl:for-each select="/root/tbody/tr[position() &lt; $total_num]">
				
					<xsl:variable name="rowNum" select="position()+1"/>
					
					<xsl:for-each select="td[position() &lt; last()]"> 
						<xsl:variable name="c" select="position()"/>
						
						<xsl:if test="$c mod $colNum = 0 or $c=$totCol">
							<tr noWrap='true'>
								<xsl:if test="$c mod $colNum = 0 and $c != $totCol">
									<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'��')">
												<xsl:value-of select="substring-before(. ,'��')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum = 0 and $c = $totCol">
									<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'��')">
												<xsl:value-of select="substring-before(. ,'��')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum != 0 and $c = $totCol">
									<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; $c - ($c mod $colNum)  and position() &lt; $c + 1 ]">
										<td  nowrap='true' style='fontsize:coltitle'>
											<xsl:if test="$c &gt; $colNum"><xsl:attribute name="style">pagebreak:deny</xsl:attribute></xsl:if>
											<xsl:if test="contains( . ,'��')">
												<xsl:value-of select="substring-before(. ,'��')"/>
											</xsl:if>
											<xsl:if test="not(contains( . ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
							</tr>
							
							<tr noWrap='true'>
								<xsl:if test="$c mod $colNum = 0 and $c != $totCol">
									<xsl:for-each select="/root/tbody/tr[$rowNum]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<xsl:variable name="tc" select="$c - $colNum + position() "/>
										<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
											<xsl:if test="contains( /root/tbody/tr[1]/td[$tc] ,'��')">
												<xsl:if test=".!=''">
													<xsl:attribute name="align">right</xsl:attribute>
													<xsl:value-of select="format-number(.,substring-after(/root/tbody/tr[1]/td[$tc] ,'��'))"/>
												</xsl:if>
												<xsl:if test=".=''">
													<xsl:attribute name="align">right</xsl:attribute>0.00
												</xsl:if>
											</xsl:if>
											<xsl:if test="not(contains( /root/tbody/tr[1]/td[$tc] ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum = 0 and $c = $totCol">
									<xsl:for-each select="/root/tbody/tr[$rowNum ]/td[position() &gt; $c - $colNum and position() &lt; $c + 1]">
										<xsl:variable name="tc" select="$c - $colNum + position() "/>
										<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
											<xsl:if test="contains( /root/tbody/tr[1]/td[$tc] ,'��')">
												<xsl:if test=".!=''">
													<xsl:attribute name="align">right</xsl:attribute>
													<xsl:value-of select="format-number(.,substring-after(/root/tbody/tr[1]/td[$tc] ,'��'))"/>
												</xsl:if>
												<xsl:if test=".=''">
													<xsl:attribute name="align">right</xsl:attribute>0.00
												</xsl:if>
											</xsl:if>
											<xsl:if test="not(contains( /root/tbody/tr[1]/td[$tc] ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
								<xsl:if test="$c mod $colNum != 0 and $c = $totCol">
									<xsl:for-each select="/root/tbody/tr[$rowNum]/td[position() &gt; $c - ($c mod $colNum)  and position() &lt; $c + 1 ]">
										<xsl:variable name="tc" select="$c - ($c mod $colNum) + position() "/>
										<td  nowrap='true' style='fontsize:coltitle;pagebreak:deny'>
											<xsl:if test="contains( /root/tbody/tr[1]/td[$tc] ,'��')">
												<xsl:if test=".!=''">
													<xsl:attribute name="align">right</xsl:attribute>
													<xsl:value-of select="format-number(.,substring-after(/root/tbody/tr[1]/td[$tc] ,'��'))"/>
												</xsl:if>
												<xsl:if test=".=''">
													<xsl:attribute name="align">right</xsl:attribute>0.00
												</xsl:if>
											</xsl:if>
											<xsl:if test="not(contains( /root/tbody/tr[1]/td[$tc] ,'��'))">
												<xsl:value-of select="."/>
											</xsl:if>
										</td>
									</xsl:for-each>
								</xsl:if>
							</tr>
						</xsl:if>
					</xsl:for-each>
					<xsl:if test="/root/annex/annex_emptyrow = 1 ">
						<tr>
							<td height="3" style="pagebreak:deny"><xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute></td>
						</tr>
					</xsl:if>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
