<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='0.00'/>
	<xsl:template match="/">
		<thead>
			<xsl:variable name="VHSCALE" select="/root/annex/VHSCALE"/>
			<xsl:variable name="bodyrows" select="/root/tbody/tr"/>
			<xsl:for-each select="/root/t2head/tr">
	    	<xsl:choose>
	    		<xsl:when test=" position() &lt; 3 ">
	    			<xsl:variable name="rows" select="position()"/>
	    			<tr noWrap='true' class='mainHead'>
	    				<xsl:if test=" $rows =1 ">
	    					<th  rowspan="2" valign="middle" width='60' >选择</th>
	          		<th  rowspan="2" valign="middle" width='60' ><xsl:value-of select="/root/t2head/tr[1]/td[1]"/></th>
	          		<th  rowspan="2" valign="middle" width='60' ><xsl:value-of select="/root/t2head/tr[1]/td[2]"/></th>
          		</xsl:if>
          		<xsl:if test=" $rows =2 ">
          			<th style="display:none"></th>
	          		<th style="display:none" ></th>
	          		<th style="display:none" ></th>
          		</xsl:if>
		          <xsl:for-each select="td">
		          	<xsl:variable name="cols" select="position()"/>
		          	<xsl:variable name="cols_name" select="."/>
		          	<xsl:variable name="colNums" select="count(/root/t2head/tr[ $rows ]/td[.= $cols_name]) "/>
		          	<xsl:choose>
			          	<xsl:when test=" position()>4">
			          		<xsl:if test=" $rows=1">
				          		<xsl:if test=" ../td[$cols]!= ../td[$cols -1 ]">
				          			<th>
				          				<xsl:attribute name="colspan" >
				          				<xsl:value-of select="$colNums"/>
						            	</xsl:attribute>
						            	<xsl:value-of select="."/>
				          			</th>
					          	</xsl:if>
					          	<xsl:if test=" ../td[$cols] = ../td[$cols -1 ]">
				          			<th style="display:none" >
						            	<xsl:value-of select="."/>
				          			</th>
					          	</xsl:if>
					          </xsl:if>
					          <xsl:if test=" $rows=2">
			          			<th>
					            	<xsl:value-of select="."/>
			          			</th>
					          </xsl:if>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		          <xsl:if test=" $rows =1 ">
	          		<th rowspan="2" valign="middle" width='60' >合计</th>
          		</xsl:if>
          		<xsl:if test=" $rows =2 ">
	          		<th style="display:none" >合计</th>
          		</xsl:if>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
		</thead>                 
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="row_index" select="position()"/>
	    	<xsl:choose>
	    		<xsl:when test=" position()>0 ">
	    			<tr>
	    				<td>
	    					<input type='radio' onclick="selectRow(this)" name='rowradio'>
									<xsl:attribute name="value"><xsl:value-of select="$row_index"/></xsl:attribute>
								</input>
    					</td>
		          <xsl:for-each select="td">
		          	<xsl:choose>
		          		<xsl:when test=" position()=1 ">
			            	<td width="60"><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:when test=" position()=2 ">
			            	<td width="80"><xsl:value-of select="."/></td>
			            </xsl:when>
			          	<xsl:when test="position() > 4 ">
			            	<td  align="right" ><xsl:value-of select="."/></td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			            <xsl:when test="position()=4">
			            	<td  align="right" ><xsl:value-of select="."/></td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
   		<tr>
   			<td colspan="3" >合计</td>
   			<td style="display:none" ></td>
   			<td style="display:none" ></td>
   			<xsl:for-each select="/root/t2head/tr">
   				<xsl:variable name="last_row" select="last()"/>
        	<xsl:choose>
        		<xsl:when test="position() =$last_row">
	        		<xsl:for-each select="td">
	        			<xsl:choose>
			          	<xsl:when test="position()> 4 ">
			            	<td align="right" ><xsl:value-of select="format-number(.,'####0.00')"/></td>
			            </xsl:when>
		            </xsl:choose>
	            </xsl:for-each>
	          </xsl:when>
        	</xsl:choose>
	   		</xsl:for-each>
	   		<td align="right" >
          <xsl:value-of select="format-number(/root/t2head/tr[3]/td[4],'####0.00')"/>
		  	</td>
   		</tr>
		</tbody>
	</xsl:template>
</xsl:stylesheet>