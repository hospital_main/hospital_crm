<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'></th>
				<th nowrap='true'>转账方案编码</th>
				<th nowrap='true'>转账方案名称</th> 
			</tr>     
		</thead>  
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				
				<xsl:for-each select="td">   
					
						<xsl:choose>
							<xsl:when test="position() = 1">
								<td align="center">
									<input type='radio' name="radioBtn"/>
								</td>
								<td>
									<xsl:value-of select="."/> 
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
								<xsl:value-of select="."/> 
								</td> 
							</xsl:otherwise>
						</xsl:choose>
					
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>