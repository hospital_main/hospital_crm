<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>科目编码</th>
				<th nowrap='true'>科目名称</th> 
				<th nowrap='true'>部门</th> 
				<th nowrap='true'>人员类别</th> 
				<th nowrap='true'>工资额</th> 
			</tr>     
		</thead>       
	<tbody>     
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">   
					<td>
						<xsl:choose>
							<xsl:when test="position() = 1 ">
								<xsl:value-of select="."/>
							</xsl:when>
							<xsl:when test="position() = 5">
								<xsl:attribute name="align">right</xsl:attribute>
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

