<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style="colspan:colcount;fontsize:maintitle"></td>
					<td nowrap='true' style="display:none"></td>
					<td nowrap='true' style="display:none"></td>
					<td nowrap='true' style="display:none"></td>
					<td nowrap='true' style="display:none"></td>
				</tr>   
				<tr noWrap='true' class='mainHead'>
					<td nowrap='true'>科目编码</td>
					<td nowrap='true'>科目名称</td> 
					<td nowrap='true'>部门</td> 
					<td nowrap='true'>人员类别</td> 
					<td nowrap='true'>工资额</td> 
				</tr>   
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">   
							<td>
								<xsl:choose>
									<xsl:when test="position() = 1 ">
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position() = 5">
										<xsl:attribute name="align">right</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="."/>  
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
