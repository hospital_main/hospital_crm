<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
  		<xsl:for-each select="/root/tbody/tr">
  			<xsl:choose>
	    		<xsl:when test=" position() &lt; 2 ">
		  			<tr noWrap='true' class='mainHead'>
		  				<xsl:for-each select="td">
		  					<xsl:choose>
								<xsl:when test=" position()>3 ">
			  					<th valign="middle">
			  						<xsl:value-of select="."/>
									</th>
									</xsl:when>
								</xsl:choose>
							</xsl:for-each>
						</tr>
					</xsl:when>
				</xsl:choose>
				</xsl:for-each>
  	</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr ">
				<xsl:variable name="rows" select="position()"/>
				<xsl:variable name="cols_name" select="td[1]"/>
	      <xsl:variable name="rowNums" select="count(/root/tbody/tr/td[.=$cols_name]) "/>
				<xsl:choose>
					<xsl:when test=" position()=1">
					</xsl:when>
					<xsl:when test=" position()&gt;1">
						<tr>
							<xsl:for-each select="td">
								<xsl:variable name="cols" select="position()"/>
			        	
								<xsl:choose>
					          	<xsl:when test=" position()&lt;4 ">
					          	</xsl:when>
					          	<xsl:when test=" position()=4 ">
					          		
					          			<xsl:if test="../td[1]!= ../../tr[$rows -1]/td[1]">
						          			<td >
						          				<xsl:attribute name="rowspan" >
						          				<xsl:value-of select="$rowNums"/>
								            	</xsl:attribute>
								            	<xsl:value-of select="."/>
						          			</td>
							          	</xsl:if>
							          	<xsl:if test=" ../td[1] = ../../tr[$rows -1]/td[1]">
						          			<td style="display:none" >
								            	<xsl:value-of select="."/>
						          			</td>
							          	</xsl:if>
					          	</xsl:when>
					          	<xsl:when test=" position()>5 ">
					          		<td align="right"><xsl:value-of select="format-number(.,'##,##0.00')"/></td>
											</xsl:when>
					          	<xsl:otherwise>
					          		<td ><xsl:value-of select="."/></td>
					          		</xsl:otherwise>
			            </xsl:choose>
							</xsl:for-each>
						</tr>
				</xsl:when>
				</xsl:choose>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>