<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='0.00'/>
	<xsl:template match="/">
		<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-5" />
		<root>
		<thead>
			<xsl:variable name="VHSCALE" select="/root/annex/VHSCALE"/>
			<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
			<xsl:for-each select="/root/tbody/tr">
  			<xsl:choose>
	    		<xsl:when test=" position() &lt; 2 ">
		  			<tr noWrap='true' class='mainHead'>
		  				<xsl:for-each select="td">
		  					<xsl:choose>
								<xsl:when test=" position()>4 ">
			  					<td valign="middle">
			  						<xsl:value-of select="."/>
									</td>
									</xsl:when>
								</xsl:choose>
							</xsl:for-each>
						</tr>
					</xsl:when>
				</xsl:choose>
				</xsl:for-each>
  	</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr ">
				<xsl:variable name="rows" select="position()"/>
				<xsl:variable name="cols_name" select="td[6]"/>
	      <xsl:variable name="rowNums" select="count(/root/tbody/tr/td[.=$cols_name]) "/>
				<xsl:choose>
					<xsl:when test=" position()=1">
					</xsl:when>
					<xsl:when test=" position()&gt;1">
						<tr>
							<xsl:for-each select="td">
								<xsl:variable name="cols" select="position()"/>
								<xsl:choose>
					          	<xsl:when test=" position()&lt;5 ">
					          	</xsl:when>
					          	<xsl:when test=" position()=5 or position()=6 or position()=7 or position()=8">
					          			<xsl:if test="../td[6]!= ../../tr[$rows -1]/td[6]">
						          			<td >
						          				<xsl:attribute name="rowspan" >
						          				<xsl:value-of select="$rowNums"/>
								            	</xsl:attribute>
								            	<xsl:value-of select="."/>
						          			</td>
							          	</xsl:if>
							          	<xsl:if test=" ../td[6] = ../../tr[$rows -1]/td[6]">
						          			<td style="display:none" >
								            	<xsl:value-of select="."/>
						          			</td>
							          	</xsl:if>
					          	</xsl:when>
					          	<xsl:when test=" position()>9 ">
					          		<td align="right"><xsl:value-of select="format-number(.,'##,##0.00')"/></td>
											</xsl:when>
					          	<xsl:otherwise>
					          		<td ><xsl:value-of select="."/></td>
					          		</xsl:otherwise>
			            </xsl:choose>
							</xsl:for-each>
						</tr>
				</xsl:when>
				</xsl:choose>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>