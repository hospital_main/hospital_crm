<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='0.00'/>
	<xsl:template match="/">
		<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-2" />
		<root>
		<thead>
			<xsl:variable name="bodyrows" select="/root/tbody/tr"/>
			<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>  
        </xsl:call-template>
  		</tr>
			<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=1">
	    			<tr noWrap='true' class='mainHead'>

          		<td valign="middle" width='60' ><xsl:value-of select="/root/tbody/tr[1]/td[1]"/></td>
          		<td valign="middle" width='60' ><xsl:value-of select="/root/tbody/tr[1]/td[2]"/></td>
		          <xsl:for-each select="td">
		          	<xsl:variable name="cols" select="position()"/>
		          	<xsl:variable name="cols_name" select="."/>
		          	<xsl:choose>
			          	<xsl:when test=" position()>3">
		          			<td>
				            	<xsl:value-of select="."/>
		          			</td>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
		</thead>                 
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="row_index" select="position()"/>
	    	<xsl:choose>
	    		<xsl:when test=" position()>1  and position()!=last() ">
	    			<tr>

		          <xsl:for-each select="td">
		          	<xsl:choose>
		          		<xsl:when test="position()=1 ">
			            	<td width="60"><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:when test="position()=2 ">
			            	<td width="80"><xsl:value-of select="."/></td>
			            </xsl:when>
			          	<xsl:when test="position() > 3 ">
			            	<td  align="right" ><xsl:value-of select="."/></td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
   		<tr>
   			<td colspan="2">�ϼƣ�</td>

   			<td style="display:none" ></td>
   			<xsl:for-each select="/root/tbody/tr">
   				<xsl:variable name="last_row" select="last()"/>
        	<xsl:choose>
        		<xsl:when test="position() =$last_row">
	        		<xsl:for-each select="td">
	        			<xsl:choose>
			          	<xsl:when test="position()> 3 ">
			            	<td align="right" ><xsl:value-of select="."/></td>
			            </xsl:when>
		            </xsl:choose>
	            </xsl:for-each>
	          </xsl:when>
        	</xsl:choose>
	   		</xsl:for-each>
   		</tr>
		</tbody>
		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>