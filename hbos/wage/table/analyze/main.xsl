<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='0.00'/>
	<xsl:template match="/">
		<thead>
			<xsl:variable name="bodyrows" select="/root/tbody/tr"/>
			<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=1 ">
	    			<tr noWrap='true' class='mainHead'>
    					<th valign="middle" width='60' >ѡ��</th>
          		<th valign="middle" width='60' ><xsl:value-of select="/root/tbody/tr[1]/td[1]"/></th>
          		<th valign="middle" width='60' ><xsl:value-of select="/root/tbody/tr[1]/td[2]"/></th>
		          <xsl:for-each select="td">
		          	<xsl:variable name="cols" select="position()"/>
		          	<xsl:variable name="cols_name" select="."/>
		          	<xsl:choose>
			          	<xsl:when test=" position()>3">
		          			<th>
				            	<xsl:value-of select="."/>
		          			</th>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
		</thead>                 
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="row_index" select="position()"/>
	    	<xsl:choose>
	    		<xsl:when test=" position()>1  and position()!=last() ">
	    			<tr>
	    				<td>
	    					<input type='radio' onclick="selectRow(this)" name='rowradio'>
									<xsl:attribute name="value"><xsl:value-of select="$row_index"/></xsl:attribute>
								</input>
    					</td>
		          <xsl:for-each select="td">
		          	<xsl:choose>
		          		<xsl:when test="position()=1 ">
			            	<td width="60"><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:when test="position()=2 ">
			            	<td width="80"><xsl:value-of select="."/></td>
			            </xsl:when>
			          	<xsl:when test="position() > 3 ">
			            	<td  align="right" ><xsl:value-of select="."/></td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
   		<tr>
   			<td colspan="3" align="left">�ϼƣ�</td>
   			<td style="display:none" ></td>
   			<td style="display:none" ></td>
   			<xsl:for-each select="/root/tbody/tr">
   				<xsl:variable name="last_row" select="last()"/>
        	<xsl:choose>
        		<xsl:when test="position() =$last_row">
	        		<xsl:for-each select="td">
	        			<xsl:choose>
			          	<xsl:when test="position()> 3 ">
			            	<td align="right" ><xsl:value-of select="."/></td>
			            </xsl:when>
		            </xsl:choose>
	            </xsl:for-each>
	          </xsl:when>
        	</xsl:choose>
	   		</xsl:for-each>
   		</tr>
		</tbody>
	</xsl:template>
</xsl:stylesheet>