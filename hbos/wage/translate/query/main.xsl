<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>凭证编号</th>
				<th nowrap='true'>凭证日期</th> 
				<th nowrap='true'>附件</th> 
				<th nowrap='true'>摘要</th> 
				<th nowrap='true'>科目</th> 
				<th nowrap='true'>借方金额</th>
				<th nowrap='true'>贷方金额</th> 
				<th nowrap='true'>制单人</th> 
				<th nowrap='true'>审核人</th> 
				<th nowrap='true'>记账人</th>  
				<th nowrap='true'>作废</th> 
			</tr>     
		</thead>       
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">   
					<td>
						<xsl:choose>
							<xsl:when test="position() = 1 ">
								<a href="#">
									<xsl:attribute name="onclick">
										javascript:openMyDlg(<xsl:value-of select="position()"/>,"<xsl:value-of select="../pk/vouch_id"/>")
									</xsl:attribute><xsl:value-of select="."/>
								</a>
							</xsl:when>
							<xsl:when test="position() = 6 or position()=7">
								<xsl:if test=".!=0">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

