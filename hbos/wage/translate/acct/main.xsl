<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th nowrap='true'>编码</th>
				<th nowrap='true'>转账方案名称</th> 
				<th nowrap='true'>摘要</th> 
				<th nowrap='true'>凭证类型</th> 
				<th nowrap='true'>凭证序号</th> 
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<td align='center'  style='display:none'>
					<input type='checkbox' TABINDEX='-1'>
						<xsl:attribute name="pk" ><xsl:value-of select="pk/voucher_code"/></xsl:attribute>
					</input>
				</td>
				<xsl:for-each select="td[position()!=6]">   
					<td>
						<xsl:choose>
							<xsl:when test="position() = 1">
								<a href="#">
									<xsl:attribute name="onclick">
										javascript:openMyDlg(<xsl:value-of select="position()"/>,"<xsl:value-of select="../pk/voucher_code"/>")
									</xsl:attribute><xsl:value-of select="."/>
								</a>
							</xsl:when>
							<xsl:when test="position() = 1">
								<a href="#">
									<xsl:attribute name="onclick">
										javascript:openMyDlg(<xsl:value-of select="position()"/>,"<xsl:value-of select="."/>")
									</xsl:attribute><xsl:value-of select="."/>
								</a>
							</xsl:when>
							<xsl:when test="position()=5">
								<xsl:attribute name="vid"><xsl:value-of select="../td[6]"/></xsl:attribute>
								<a href="#">
									<xsl:attribute name="onclick">
										javascript:openMyDlg(<xsl:value-of select="position()"/>,"<xsl:value-of select="../td[6]"/>")
									</xsl:attribute><xsl:value-of select="."/>
								</a>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

