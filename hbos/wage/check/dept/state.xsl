<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
			<tr noWrap="true" class="mainHead"> 
			  <th style='display:none' width='25'><input type='checkbox'/></th>
				<th noWrap="true" >科室编码</th> 
				<th noWrap="true" >科室名称</th>
				<th noWrap="true" >医生考勤</th>
				<th noWrap="true" >护士考勤</th>
				<th noWrap="true" >其他考勤</th>
		  </tr>
		</thead>
		
    <tbody/>
 
	</xsl:template>
</xsl:stylesheet>
