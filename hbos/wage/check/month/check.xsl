<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
			<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true" >职工编码</th> 
				<th noWrap="true" >职工姓名</th>
				<th noWrap="true" >科室编码</th>
				<th noWrap="true" >科室名称</th>
				<th noWrap="true" >职工类别</th>
				<th noWrap="true" >考勤类别</th>
				<th noWrap="true" >是否轮转</th>
				<xsl:for-each select="/root/tbody/tr[td[1]='']/td[position() &gt; 7]">
					<th noWrap="true" ><xsl:value-of select="."/></th>
				</xsl:for-each>
		  </tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[td[1] != '']">
				<tr>     
					<xsl:for-each select="td">
						<td>
							<xsl:value-of select="."/>
						</td>	
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
