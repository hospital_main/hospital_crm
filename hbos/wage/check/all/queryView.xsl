<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/wage/check/all/queryView.xsl,v 1.1 2012/03/12 01:56:21 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:56:21 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<thead>
				<tr noWrap='true'>
					<td style="colspan:colcount;fontsize:maintitle"></td>
					<xsl:for-each select="/root/tbody/tr[1]/td[position()&gt;1]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
				</tr>
				<tr noWrap='true'>
					<xsl:for-each select="/root/tbody/tr[1]/td">
						<td  nowrap='true' style='fontsize:coltitle'><xsl:value-of select="."/></td>
					</xsl:for-each>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr[position()>1]">
					<tr>
						<xsl:for-each select="td"> 
							<xsl:variable name="c" select="position()"/>
							<xsl:if test="string-length($c)&gt;0">
								<xsl:if test="substring( /root/tbody/tr[1]/td[$c] ,string-length( /root/tbody/tr[1]/td[$c] ),1)=' '">
									<td align="right"><xsl:value-of select="format-number(.,'##,##0.00')"/></td>
								</xsl:if>
								<xsl:if test="substring( /root/tbody/tr[1]/td[$c] ,string-length( /root/tbody/tr[1]/td[$c] ),1)!=' '">
									<td><xsl:value-of select="."/></td>
								</xsl:if>
							</xsl:if>
							<xsl:if test="string-length($c)=0">
								<td></td>
							</xsl:if>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
