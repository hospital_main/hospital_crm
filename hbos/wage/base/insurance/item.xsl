<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>险种编码</th>
				<th nowrap='true'>险种</th> 
				<th nowrap='true'>缴费人员类别</th> 
				<!--<th nowrap='true'>个人工资项</th> -->
			</tr>     
		</thead>        
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position() &gt; 4]">   
						<td>
							<xsl:choose>
								<xsl:when test="position() = 1">
									<xsl:attribute name="align">center</xsl:attribute>
									<a >										
										<xsl:attribute name="href">
											javascript:loadData("<xsl:for-each select="../td[position()=1 or position()=2 or position()=3 or position()=4]">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
										</xsl:attribute><xsl:value-of select="."/>
									</a>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>  
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>

