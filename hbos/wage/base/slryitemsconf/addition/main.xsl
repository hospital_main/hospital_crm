<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th rowspan="2"  valign='middle'><input type='checkbox' value=''/></th>
		<th nowrap='true'  valign='middle'>年月</th>
      	<th nowrap='true'  valign='middle'>职工编码</th>
      	<th nowrap='true'  valign='middle'>职工名称</th>
		<th nowrap='true'  valign='middle'>部门</th>
		<th nowrap='true'  valign='middle'>补扣项总金额</th>
		<th nowrap='true'  valign='middle'>子女教育</th>
		<th nowrap='true'  valign='middle'>住房贷款利息</th>
		<th nowrap='true'  valign='middle'>继续教育</th>
		<th nowrap='true'  valign='middle'>住房租金</th>
		<th nowrap='true'  valign='middle'>赡养老人</th>
		<th nowrap='true'  valign='middle'>描述</th>
		<th nowrap='true'  valign='middle'>状态</th>
		<th nowrap='true'  valign='middle'>审批人</th>	
      </tr>
    </thead>

    <tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
         <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                  <a href="#">
						<xsl:attribute name="onclick">
							javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:23;dialogHeight:350px')
						</xsl:attribute>
                 		<xsl:value-of select="."/>
                  </a>
                </td>
              </xsl:when>
             
			 <xsl:when test="position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or  position()=10 ">
					<td align="right"><xsl:value-of select="format-number(., '#,##0.00')"/></td>
					
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
			  
			  
            </xsl:choose>            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>

  </xsl:template>

</xsl:stylesheet>



