<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th rowspan="2" valign='middle'><input type='checkbox' value=''/></th>
      	<th nowrap='true' rowspan='2' valign='middle'>职工编码</th>
      	<th nowrap='true' rowspan='2' valign='middle'>职工名称</th>
		<th nowrap='true' rowspan='2' valign='middle'>部门</th>
		<th nowrap='true' colspan='3' valign='middle'>子女教育</th>
		<th nowrap='true' colspan='3' valign='middle'>住房贷款利息</th>
		<th nowrap='true' colspan='3' valign='middle'>继续教育</th>
		<th nowrap='true' colspan='3' valign='middle'>住房租金</th>
		<th nowrap='true' colspan='3' valign='middle'>赡养老人</th>
		<th nowrap='true' rowspan='2' valign='middle'>状态</th>
		<th nowrap='true' rowspan='2' valign='middle'>审批人</th>	
      </tr>
	   <tr noWrap='true' class='mainHead'>
      	<th nowrap='true' >金额</th>
		<th nowrap='true' >开始时间</th>
		<th nowrap='true' >结束时间</th>
		<th nowrap='true' >金额</th>
		<th nowrap='true' >开始时间</th>
		<th nowrap='true' >结束时间</th>
		<th nowrap='true' >金额</th>
		<th nowrap='true' >开始时间</th>
		<th nowrap='true' >结束时间</th>
		<th nowrap='true' >金额</th>
		<th nowrap='true' >开始时间</th>
		<th nowrap='true' >结束时间</th>
		<th nowrap='true' >金额</th>
		<th nowrap='true' >开始时间</th>
		<th nowrap='true' >结束时间</th>
		
      </tr>
	  
    </thead>

    <tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
         <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                  <a href="#">
						<xsl:attribute name="onclick">
							javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:720px;dialogHeight:580px')
						</xsl:attribute>
                 		<xsl:value-of select="."/>
                  </a>
                </td>
              </xsl:when>
             
			 <xsl:when test="position()=4 or position()=7 or position()=10 or position()=13 or position()=16">
					<td align="right"><xsl:value-of select="format-number(., '#,##0.00')"/></td>
					
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
			  
			  
            </xsl:choose>            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>

  </xsl:template>

</xsl:stylesheet>



