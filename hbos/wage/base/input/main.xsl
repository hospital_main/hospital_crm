<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th>职工编码</th>
      	<th>职工姓名</th>
      	<th>部门</th>
      	<th>人员类别</th>
      	<th>方案名称</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:if test="position()!=5">
              <td align='left'><xsl:value-of select="."/></td>
            </xsl:if>
            <xsl:if test="position()=5">
              <td align='left'>
                <xsl:value-of select="."/>　
								<a href='#'>
									<xsl:attribute name="onclick">
										javascript:setThisScheme("&lt;emp_code&gt;<xsl:value-of select="../td[1]"/>&lt;/emp_code&gt;&lt;emp_name&gt;<xsl:value-of select="../td[2]"/>&lt;/emp_name&gt;&lt;scheme&gt;<xsl:value-of select="."/>&lt;/scheme&gt;")
									</xsl:attribute>
										设置
									</a>              
							</td>
            </xsl:if>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

