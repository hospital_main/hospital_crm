<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<table>
		<thead>
  		<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test=" position() &lt; 3 ">
	    			<xsl:variable name="rows" select="position()"/>
	    			<tr noWrap='true'>
	    				<xsl:if test=" $rows=1">
          			<td  rowspan="2" valign="middle" width='60' ><xsl:value-of select="/root/tbody/tr[1]/td[1]"/></td>
          		</xsl:if>
          		<xsl:if test=" $rows=2">
          			<td  style="display:none" ><xsl:value-of select="/root/tbody/tr[1]/td[1]"/></td>
          		</xsl:if>
		          <xsl:for-each select="td">
		          	<xsl:variable name="cols" select="position()"/>
		          	<xsl:variable name="cols_name" select="."/>
		          	<xsl:variable name="colNums" select="count(/root/tbody/tr[ $rows ]/td[.= $cols_name]) "/>
		          	<xsl:choose>
			          	<xsl:when test=" position()>1 and position ()!=5 and position ()!=4 ">
			          		<xsl:if test=" $rows=1">
				          		<xsl:if test=" ../td[$cols]!= ../td[$cols -1 ]">
				          			<td valign="middle">
				          				<xsl:attribute name="colspan" >
				          				<xsl:value-of select="$colNums"/>
						            	</xsl:attribute>
						            	<xsl:if test=" ../td[$cols]= ../../tr[2]/td[$cols ]">
							            	<xsl:attribute name="rowspan" >
					          					<xsl:value-of select="2"/>
							            	</xsl:attribute>
						            	</xsl:if>
						            	<xsl:value-of select="."/>
				          			</td>
					          	</xsl:if>
					          	<xsl:if test=" ../td[$cols] = ../td[$cols -1 ]">
				          			<td style="display:none" >
						            	<xsl:value-of select="."/>
				          			</td>
					          	</xsl:if>
					          </xsl:if>
					          <xsl:if test=" $rows=2">					          	
					          	<td valign="middle" width='60'><xsl:value-of select="."/></td>         	
					          </xsl:if>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		          <xsl:if test=" $rows =1 ">
		          	<td rowspan="2" valign="middle" width='60'><xsl:value-of select="/root/tbody/tr[1]/td[5]"/></td>
          		</xsl:if>
          		<xsl:if test=" $rows =2 ">
	          		<td style="display:none"></td>
          		</xsl:if>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
  	</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="row_index" select="position()"/>
	    	<xsl:choose>
	    		<xsl:when test=" position()>2 ">
	    			<tr>
		          <xsl:for-each select="td">
		          	<xsl:choose>
		          		<xsl:when test=" position()=1 or position()=2 or position()=3 ">
			            	<td width="60"><xsl:value-of select="."/></td>
			            </xsl:when>
			          	<xsl:when test="position() > 5 ">
			            	<td  align="right" ><xsl:value-of select="."/></td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			            <xsl:when test="position()=5">
			            	<td  align="right" ><xsl:value-of select="."/></td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
   		
		</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>