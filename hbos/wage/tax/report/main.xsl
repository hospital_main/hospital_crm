<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th align="center" colspan="19">扣缴个人所得税报告表</th>
      	<th style="display:none"></th><th style="display:none"></th><th style="display:none"></th>
      	<th style="display:none"></th><th style="display:none"></th><th style="display:none"></th>
      	<th style="display:none"></th><th style="display:none"></th><th style="display:none"></th>
      	<th style="display:none"></th><th style="display:none"></th><th style="display:none"></th>
      	<th style="display:none"></th><th style="display:none"></th><th style="display:none"></th>
      	<th style="display:none"></th><th style="display:none"></th><th style="display:none"></th>
      </tr>
    </thead>
    <tbody>
    	<tr>
    		<td colspan="9">扣缴义务人编码：<xsl:value-of select="/root/tbody/tr[1]/td[1]"/></td>
    		<td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>
    		<td colspan="10">填报日期：<xsl:value-of select="/root/tbody/tr[1]/td[2]"/></td>
    		<td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td>
    	</tr>	
    	<tr>
    		<td colspan="19" align="right">金额单位：人民币元</td>
    		<td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td>
    	</tr>
    	<tr>
    		<td colspan="19" align="left">根据《中华人民共和国个人所得税法》第九条的规定，制定本表，扣缴义务人应将本月扣缴的税款在次月七日内缴入国库，并向当地税务机关报送本表。</td>
    		<td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td>
    	</tr>
    	<tr>
    		<td colspan="7" align="left">扣缴义务人名称：<xsl:value-of select="/root/tbody/tr[1]/td[3]"/></td>
    		<td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td>
    		
    		
    		<td colspan="7">地址：<xsl:value-of select="/root/tbody/tr[1]/td[4]"/></td>
    		<td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>
    		<td colspan="5">电话：<xsl:value-of select="/root/tbody/tr[1]/td[5]"/></td>
    		<td style="display:none"></td><td style="display:none"></td><td style="display:none"></td>
    		<td style="display:none"></td>
    	</tr>
    	
    	<tr>
    		<td rowspan="3">纳税义务人姓名</td>
    		<td rowspan="3">纳税人编码</td>
    		<td rowspan="3">工作单位和地址</td>
    		<td rowspan="3">所得项目</td>
    		<td rowspan="3">所得时间</td>
    		
    		<td colspan="6" align="center">收入额</td>
	    		<td style="display:none"></td><td style="display:none"></td>
	    		<td style="display:none"></td><td style="display:none"></td>
	    		<td style="display:none"></td>
    		
    		<td rowspan="3">减费用额</td>
    		<td rowspan="3">应纳税所得额</td>
    		<td rowspan="3">税率</td>
    		<td rowspan="3">速算扣除数</td>
    		<td rowspan="3">扣缴所得税额</td>
    		<td rowspan="3">完税证字号</td>
    		<td rowspan="3">五险一金</td>
    		<td rowspan="3">纳税日期</td>
    		
    	</tr>
    	<tr>
    		<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td rowspan="2">人民币</td>
	    	
	    	<td colspan="4" align="center">外币</td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td rowspan="2">人民币合计</td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	
    	</tr>
    	<tr>
    		<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td>货币名称</td>
	    	<td>金额</td>
	    	<td>外汇牌价</td>
	    	<td>折合人民币</td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    </tr>	
    	
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
                    <xsl:choose>
                      <xsl:when test="position() = 6 or position() = 7 or position() = 8 or position() = 9 or position() = 10 or position() = 22 or position() = 24 ">
                        <td align='left'>
                          <xsl:value-of select="."/><!--format-number(.,'#,##0.00')-->
                        </td>
                      </xsl:when>
                      
                      <xsl:when test="position() = 11 or position() = 12 or position() = 13 or position() = 14 or position() = 15 or position() = 16 or position() = 17 or position() = 18 or position() = 19 or position() = 20  or position() = 21 or position() = 23">
                        <td align='right'>
                          <xsl:value-of select="format-number(.,'#,##0.00')"/>
                        </td>
                      </xsl:when>
                      
                      <!--
                      <xsl:otherwise>
                        <td align='left'><xsl:value-of select="."/></td>
                      </xsl:otherwise>
                      -->
                      
                    </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
      
      	<tr>
      		<td colspan="19">如果由扣缴义务人填写完税证，应在填报此表时附完税证副联___________份</td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      	</tr>
      	
      	<tr>
      		<td colspan="19">扣缴义务人声明 我声明：此扣缴申报表是根据《中华人民共和国个人所得税法》的规定填报的，我确信它是真实的、可靠的、完整的。 声明人签字：＿＿＿＿＿</td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      	</tr>
      	<tr>
      		<td colspan="19">
      		会计主管人签字： 负责人签字： 扣缴单位（或个人）盖章： 
      		</td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      	</tr>
      	<tr>
      		<td colspan="19">
      		以下由税务机关填写
      		</td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      	</tr>
      	<tr>
      		<td colspan="5">
      		收到日期
      		</td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		
      		
      		<td colspan="6">接受人</td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		
      		<td colspan="4">审核日期</td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		
      		
      		<td rowspan="5" colspan="4">
      		<br/>
      		主管税务机关盖章
      		<br/>
      		<br/>
      		<br/>
      		主管税务官员签字 
      		<br/>
      		</td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      	</tr>
      	<tr>
      		<td rowspan="4">
      		审核记录
      		</td>
      		<td colspan="14" rowspan="4"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      	</tr>
      	
      	<tr>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      	</tr>
      	<tr>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      	</tr>
      	<tr>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      	</tr>
      	
      	
    </tbody>
  </xsl:template>
</xsl:stylesheet>

