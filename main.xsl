<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/"> 
    <xsl:variable name="first" select="/root/@first"/>
    <xsl:variable name="second" select="/root/@second"/>
    <xsl:variable name="third" select="/root/@third"/>
    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" >
    <tr>
    <td width="130" valign="top" align="center" background="base/themes/blue/images/mainBodyGg.gif">
      <table id="_1st_menu" width="110" border="0" cellpadding="0" cellspacing="0">
        <tbody>
        <tr></tr>
          
        <xsl:for-each select="/root/menu_node">
          <tr>
            <td>
              <xsl:attribute name="class" >
                <xsl:if test="$first=position()">1stmenuchose</xsl:if>
                <xsl:if test="$first!=position()">1stmenunochose</xsl:if>
      			  </xsl:attribute>
      			  <xsl:attribute name="onclick" >drawmenu(<xsl:value-of select="position()"/>, 1, 1)</xsl:attribute>
            <div nowrap="true"><xsl:value-of select="@name"/></div></td>
          </tr>
          <xsl:if test="$first=position()">
            <xsl:for-each select="menu_node">
            <tr>
              <td>
                <xsl:attribute name="class" >
                  <xsl:if test="$second=position()">2ndmenuchose</xsl:if>
                  <xsl:if test="$second!=position()">2ndmenunochose</xsl:if>
        			  </xsl:attribute>
        			  <xsl:attribute name="onclick" >drawmenu(<xsl:value-of select="$first"/>, <xsl:value-of select="position()"/>, 1)</xsl:attribute>
              <div nowrap="true"><xsl:value-of select="@name"/></div></td>
            </tr>  
           </xsl:for-each>
           <xsl:if test="$first=last()"><tr><td class="2ndmenubottom"></td></tr></xsl:if>
          </xsl:if>
        </xsl:for-each>
        <tr id="_1st_base">
        <td valign="top" nowrap="true" width="110"></td></tr>
        </tbody>
      </table>
    </td>
    <td width="87%">
      <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
        <tr id="_3rd_menu_head_" height="20">
          <xsl:attribute name="style" ><xsl:if test="count(/root/child::menu_node[position()=$first]/child::menu_node[position()=$second]/child::menu_node)=0">display:none</xsl:if></xsl:attribute>
          <td width="10"></td>
          <td>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" >
              <tr id="_3rd_menu_">
                <xsl:for-each select="/root/menu_node">
                  <xsl:if test="$first=position()">
                    <xsl:for-each select="menu_node">
                      <xsl:if test="$second=position()">
                        <xsl:for-each select="menu_node">
                          <xsl:if test="$third=position()">
                          <td><img src="base/themes/blue/images/3rdMenuChoseHeadBg.gif"/></td>
                          </xsl:if>
                          <xsl:if test="$third!=position()">
                          <td><img src="base/themes/blue/images/3rdMenuNoChoseHeadBg.gif"/></td>
                          </xsl:if>
                          <td>
                            <xsl:attribute name="class" >
                              <xsl:if test="$third=position()">3rdmenuchosebg</xsl:if>
                              <xsl:if test="$third!=position()">3rdmenunochosebg</xsl:if>
                      			</xsl:attribute>
                      			<xsl:attribute name="onclick" >drawmenu(<xsl:value-of select="$first"/>, <xsl:value-of select="$second"/>, <xsl:value-of select="position()"/>)</xsl:attribute>
                          <div nowrap="true"> <xsl:value-of select="@name"/> </div></td>
                          <xsl:if test="$third=position()">
                            <td><img src="base/themes/blue/images/3rdMenuChoseEndBg.gif"/></td>
                          </xsl:if>
                          <xsl:if test="$third!=position()">
                            <td><img src="base/themes/blue/images/3rdMenuNoChoseEndBg.gif"/></td>
                          </xsl:if>
                        </xsl:for-each>
                      </xsl:if>
                    </xsl:for-each>
                  </xsl:if>
                </xsl:for-each>
                <td class="3rdmenuendbg">
                <div nowrap="true"></div></td>
             </tr>
            </table>
          </td>
        </tr>
        <tr>
        	<td colspan="2">
        	  <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
        	    <tr>
                <td><img src="base/themes/blue/images/zuoshang02.png" width="7" height="7"/></td>
                <td background="base/themes/blue/images/shangbg02.png"></td>
              </tr>
        	    <tr>
                <td background="base/themes/blue/images/zuobg01.png" width="7"></td>
                <td height="100%" align="center" valign="center">
                  <iframe id="_iframe" name="_business" width="100%" height="100%" frameborder="0">
                    <xsl:attribute name="src" >
                      <xsl:if test="count(/root/child::menu_node[position()=$first]/child::menu_node[position()=$second]/child::menu_node)=0">
                        <xsl:value-of select="/root/child::menu_node[position()=$first]/child::menu_node[position()=$second]/page/@src"/>
                      </xsl:if>
                      <xsl:if test="count(/root/child::menu_node[position()=$first]/child::menu_node[position()=$second]/child::menu_node)!=0">
                        <xsl:value-of select="/root/child::menu_node[position()=$first]/child::menu_node[position()=$second]/child::menu_node[position()=$third]/page/@src"/>
                      </xsl:if>
              			</xsl:attribute>
                  </iframe>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  </table>
	</xsl:template>
</xsl:stylesheet>      
