<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' rowspan="2" valign="center">名次</th>
				<th nowrap='true' rowspan="2" valign="center">科室</th>
				<th nowrap='true' rowspan="2" valign="center">材料类别</th><!--骨科，介入，导管，植入--> 
				<th nowrap='true' colspan="2">材料消耗</th> 
				
			</tr>              
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' >数量</th>
				<th nowrap='true' >金额</th> 
			</tr>              
		</thead>
	<tbody>
		<xsl:for-each select="/root/data/r">
			<xsl:variable name="pos" select="position()" />
			<xsl:variable name="dept_name" select="c[2]" />
			<xsl:variable name="type_name" select="c[3]" />
			<xsl:variable name="rowspan" select="count(/root/data/r[c[2]=$dept_name])" />

			<tr>
				<xsl:for-each select="c">
					<xsl:choose>
            <xsl:when test="position()=1 or position() = 2">
							<xsl:if test="$pos = 1 or $dept_name != ../../r[$pos - 1]/c[2]">
								<xsl:if test="position() = 2">
									<td rowspan="{$rowspan}">
										<xsl:value-of select="." />
									</td>
								</xsl:if>
								<xsl:if test="position() = 1">
									<td rowspan="{$rowspan}" align="center">
										<xsl:value-of select="." />
									</td>
								</xsl:if>
							</xsl:if>
            </xsl:when>
						
						<xsl:when test="position() = 5">
							<td align="right">
								<xsl:if test="$type_name = '合计'">
									<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="format-number(.,'#,##0.00')" />
							</td>
						</xsl:when>
						
						<xsl:otherwise>
							<td>
								<xsl:if test="$type_name = '合计'">
									<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="." />
							</td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
		<!--
		<tr><td>合计</td><td align="right">1000</td><td align="right">500,000.00</td></tr>
		<tr><td>心内科一病区</td><td align="right">50</td><td align="right">20,000.00</td></tr>
		-->
	</tbody>
	</xsl:template>
</xsl:stylesheet>

